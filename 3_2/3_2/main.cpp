#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
//
////日期类
//class Date
//{
//public:
//	void Init(int year, int month, int day) {
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	void Print() {
//		cout << _year << " " << _month << " " << _day << endl;
//	}
//
//	void Print1() {
//		cout << this->_year << " " <<this-> _month << " " << this->_day << endl;
//	}
//
//private:
//	int _year;  //年
//	int _month;//月
//	int _day;//日
//};

//class A {
//public:
//	void print() {
//		cout << "hello" << endl;
//	}
//private:
//	int num;
//};

// 1.下面程序编译运行结果是？ A、编译报错 B、运行崩溃 C、正常运行
//class A
//{
//public:
//	void Print()
//	{
//		cout << "Print()" << endl;
//	}
//private:
//	int _a;
//};

// 1.下面程序编译运行结果是？ A、编译报错 B、运行崩溃 C、正常运行
class A
{
public:
	void PrintA()
	{
		cout << _a << endl;
	}
private:
	int _a;
};
int main()
{
	A* p = nullptr;
	p->PrintA();
	return 0;
}

//
//int main() {
//
//    A* t=nullptr;
//	t->print();
//
//	/*Date d1,d2;
//	d1.Init(2024, 3, 2);
//	d1.Print();
//
//	d2.Init(2024, 3, 1);
//	d2.Print();*/
//
//	
//	return 0;
//}