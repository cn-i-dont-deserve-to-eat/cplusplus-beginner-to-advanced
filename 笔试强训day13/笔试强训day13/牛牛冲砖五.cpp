#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;

int main() {
    int t;
    cin >> t;
    string str;
    while (t--) {
        int n, k;
        cin >> n >> k >> str;
        int ans = 0;
        for (int i = 0; i < n; i++) {
            if (str[i] == 'L')ans--;
            else {
                if (i >= 2 && str[i] == str[i - 1] && str[i] == str[i - 2]) {
                    ans += k;
                }
                else ans++;
            }
        }
        cout << ans << endl;
    }
}
