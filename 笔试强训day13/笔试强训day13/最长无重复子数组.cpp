#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:

    int maxLength(vector<int>& arr) {
        unordered_map<int, int> mp;
        int ans = 1;
        for (int i = 0, j = 0; i < arr.size(); i++) {
            mp[arr[i]]++;
            while (j <= i && mp[arr[i]] >= 2) {
                mp[arr[j]]--;
                j++;
            }
            ans = max(ans, i - j + 1);
        }
        return ans;
    }
};