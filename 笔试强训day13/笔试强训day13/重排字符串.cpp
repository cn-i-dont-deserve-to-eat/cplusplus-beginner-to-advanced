#include <iostream>
#include<algorithm>
#include<queue>
using namespace std;
typedef pair<int, int> PII;
int main() {
    int n;
    string str;
    int a[26] = { 0 };
    priority_queue<PII> q;
    cin >> n >> str;
    if (n == 1) {
        cout << "yes" << endl;
        cout << str << endl;
        return 0;
    }

    for (int i = 0; i < n; i++) {
        int u = str[i] - 'a';
        a[u]++;
    }
    for (int i = 0; i < 26; i++) {
        if (a[i])q.push({ a[i], i });
    }

    string ans = "";
    while (q.size() > 1) {
        auto t1 = q.top();
        q.pop();
        auto t2 = q.top();
        q.pop();
        char u1 = t1.second + 'a';
        char u2 = t2.second + 'a';

        ans += u1;
        ans += u2;
        if (t1.first - 1 > 0)q.push({ t1.first - 1, u1 - 'a' });
        if (t2.first - 1 > 0)q.push({ t2.first - 1, u2 - 'a' });
    }

    if (!q.empty() && q.top().first >= 2) {
        cout << "no" << endl;
    }
    else {
        if (!q.empty()) {
            char u = q.top().second + 'a';
            ans += u;
        }
    }
    cout << "yes" << endl;
    cout << ans << endl;


}
// 64 λ������� printf("%lld")