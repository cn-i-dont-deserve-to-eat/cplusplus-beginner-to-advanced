#define _CRT_SECURE_NO_WARNINGS 1
#include"mylist.h"
#include<iostream>
using namespace bite;
using std::cout;
using std::cin;
using std::endl;


void test1() {
	list<int> A;
	A.push_back(1);
	A.push_back(2);
	A.push_back(3);
	A.push_back(4);

	list<int>::Iterator it = A.begin();
	for (auto it : A) {
		cout << it << endl;
	}

	while (it != A.end()) {
		cout << *it << endl;
		it++;
	}
	
}

void test2() {
	list<int> A;
	A.push_back(1);
	A.push_back(2);
	A.push_back(3);
	A.push_back(4);

	list<int> B(A);
	for (auto it : B) {
		cout << it << endl;
	}
}

void test3() {
	list<int> A;
	A.push_back(1);
	A.push_back(2);
	A.push_back(3);
	A.push_back(4);
	for (auto it : A) {
		cout << it << " ";
	}
	cout << endl;

	A.erase(A.begin());
	for (auto it : A) {
		cout << it << " ";
	}
	cout << endl;
	A.erase(A.begin());
	for (auto it : A) {
		cout << it << " ";
	}
	cout << endl;
	A.erase(A.begin());
	for (auto it : A) {
		cout << it << " ";
	}
	cout << endl;
	A.erase(A.begin());
	for (auto it : A) {
		cout << it << " ";
	}
	cout << endl;
	
}


void test4() {
	list<int> A;
	A.push_back(1);
	A.push_back(2);
	A.push_back(3);
	A.push_back(4);
	/*list<int>::Iterator it = A.begin();
	while (it != A.end()) {
		cout << *it << " ";
		it++;
	}
	cout << endl;*/
	/*list<int> B((size_t)5,99);
	for (auto it : B) {
		cout << it << " ";
	}*/

	list<int> C ;
	C = A;
	for (auto it : C) {
		cout << it << " ";
	}
	cout << endl;

	cout << C.size();
	
}
void test5() {
	list<int> A;
	A.push_back(1);
	A.push_back(2);
	A.push_back(3);
	A.push_back(4);

	list<int>::Iterator it = A.begin();
	for (auto it : A) {
		cout << it << endl;
	}
	cout << endl;
	list<int>::Reserve_Iterator rit = A.rbegin();
	while (rit != A.rend()) {
		cout << *rit << endl;
		rit++;
	}
}

struct t {
	int a1;
};
void test6() {
	list<t> A;
	A.push_back({ 1 });
	A.push_back({ 2 });
	A.push_back({ 3 });

	list<t>::Iterator it = A.begin();

	while (it != A.end()) {
		//cout << (*it).a1 << endl;
		cout << it->a1 << endl;
		it++;
	}
}
int main() {
	//test1();
	//test2();
	//test3();
	//test4();
	test5();
	return 0;
}