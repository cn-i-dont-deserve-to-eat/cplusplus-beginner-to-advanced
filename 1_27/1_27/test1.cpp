#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>
#include<iostream>
namespace s1 {
	int a = 10;
	int b = 30;
}
using namespace s1;
int a = 10000;
int main()
{
	printf("%d\n", a);
	return 0;
}
//namespace myname
//{
//	// 命名空间中可以定义变量/函数/类型
//	int rand = 10;
//	int Add(int left, int right)
//	{
//		return left + right;
//	}
//	//注意：一个命名空间就定义了一个新的作用域，命名空间中的所有内容都局限于该命名空间中
//		struct Node
//	{
//		struct Node* next;
//		int val;
//	};

// 编译后后报错：error C2365: “rand”: 重定义；以前的定义是“函数”
// bit是命名空间的名字，一般开发中是用项目名字做命名空间名。
// 我们上课用的是bit，大家下去以后自己练习用自己名字缩写即可，如张三：zs
// 1. 正常的命名空间定义
//namespace bit
//{
//	// 命名空间中可以定义变量/函数/类型
//	int rand = 10;
//	int Add(int left, int right)
//	{
//		return left + right;
//	}
//}