#include"ProgressBar.h"

void download(callback_pb cb){
  //文件总大小
  double filesize=1024*1024*100;
  //当前下载量
  double current=0;
  //网络带宽
  double bandwidth=1024*1024;

  printf("download begin,current: %.1lfMb\n",current/1024/1024);

  while(current<=filesize){
    cb(filesize,current);
    usleep(100000);//单位是微秒
    current+=bandwidth;

  }
  printf("\ndownload end,current: %.1lfMb\n",filesize/1024/1024);
}
int main(){
   download(ProcBar);
  return 0;
}
