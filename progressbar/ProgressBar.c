#include"ProgressBar.h"
#define MAX_len 101
#define Style '#'

const char* lab="|/-\\";//旋转光标
void ProcBar(double tol,double cur){//tol表示要下载数据的总字节大小，cur表示当前已经下载的字节数
  int i=0;
  char a[MAX_len]={0};
  memset(a,'\0',sizeof a);
  int lab_len=strlen(lab);
  double rate=(int)(cur*100.0/tol);//当前下载了的百分比
  int target=(int)(rate);//循环次数
  while(i<=target){
    printf("[%-100s][%.1lf%%][%c]\r",a,rate,lab[i%lab_len]);
    fflush(stdout);
    a[i++]=Style;
  }
  return ;
}

//void ProcBar(){
//  int i=0;
//  char a[MAX_len]={0};
//  memset(a,'\0',sizeof a);
//  int lab_len=strlen(lab);
//  while(i<=100){
//    printf("[%-100s][%d%%][%c]\r",a,i,lab[i%lab_len]);
//    fflush(stdout);
//    a[i++]=Style;
//    usleep(50000);
//  }
//  printf("\n");
//
//  return ;
//}
