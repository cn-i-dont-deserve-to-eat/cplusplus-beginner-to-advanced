#define _CRT_SECURE_NO_WARNINGS 1
#include"Date.h"

//判断是否是闰年
bool Date::IsLeap(int year) {
	if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)){
		return true;
	}
	return false;
}
//交换两个日期
void Date::Swap(Date& d1,Date& d2) {
	swap(d1._year, d2._year);
	swap(d1._month, d2._month);
	swap(d1._day, d2._day);
}
// 全缺省的构造函数
Date::Date(int year ,int month , int day ) {
	_year = year;
	_month = month;
	_day = day;
}

// 拷贝构造函数
// d2(d1)
Date::Date(const Date& d) {
	_year = d._year;
	_month = d._month;
	_day = d._day;
}

// 赋值运算符重载
 // d2 = d3 -> d2.operator=(&d2, d3)
Date& Date::operator=(const Date& d){
	_year = d._year;
	_month = d._month;
	_day = d._day;
	return *this;
}
// 析构函数
Date::~Date() {
	_year = 0;
	_month = 0;
	_day = 0;
}
// 日期+=天数
Date& Date::operator+=(int day) {
	while (day) {
		if (_day + day <= GetMonthDay(_year, _month)) {
			_day += day;
			day -= day;
		}
		else {
			int d = GetMonthDay(_year, _month) - _day+1;
			day -= d;
			if (_month == 12) {
				_year++;
			}
			_month = (_month) % 12 + 1;
			_day = 1;
		}
	}
	return *this;
}
// 日期+天数
Date Date::operator+(int day) {
	Date temp(*this);
	while (day) {
		if (temp._day + day <= GetMonthDay(temp._year, temp._month)) {
			temp._day += day;
			day -= day;
		}
		else {
			int d = GetMonthDay(temp._year, temp._month) - temp._day + 1;
			day -= d;
			if (temp._month == 12) {
				temp._year++;
			}
			temp._month = (temp._month) % 12 + 1;
			temp._day = 1;
		}
	}
	return temp;
}
// 日期-天数
Date  Date::operator-(int day) {
	Date temp(*this);
	while (day) {
		if (temp._day - day >=1) {
			temp._day += day;
			day -= day;
		}
		else {
			if (temp._month == 1) {
				temp._year--;
			}
			day -= temp._day;
			temp._month = temp._month == 1 ? 12 : temp._month - 1;
			temp._day = GetMonthDay(temp._day, temp._month);
		}
	}
	return temp;
}
// 日期-=天数
Date& Date::operator-=(int day) {
	
	while (day) {
		if (_day - day >= 1) {
			_day += day;
			day -= day;
		}
		else {
			if (_month == 1) {
				_year--;
			}
			day -= _day;
			_month = _month == 1 ? 12 : _month - 1;
			_day = GetMonthDay(_day, _month);
		}
	}
	return *this;
}
// 前置++
Date& Date::operator++() {
	(*this) += 1;
	return *this;
}
// 后置++
Date  Date::operator++(int) {
	Date temp(*this);
	(*this)++;
	return temp;
}
// 后置--
Date  Date::operator--(int) {
	Date temp(*this);
	(*this)--;
	return temp;
}
// 前置--
Date& Date::operator--() {
	(*this) -= 1;
	return *this;
}

// >运算符重载
bool  Date::operator>(const Date& d) {
	if (_year > d._year)return true;
	else if (_year==d._year && _month > d._month)return true;
	else if (_year==d._year && _month==d._month && _day > d._day)return true;
	return false;
}
// ==运算符重载
bool  Date::operator==(const Date& d) {
	if (_year == d._year && _month == d._month && _day == d._day)return true;
	return false;
}
// >=运算符重载
bool  Date::operator >= (const Date& d) {
	if ((*this) > d || (*this) == d)return true;
	return false;
}

// <运算符重载
bool  Date::operator < (const Date& d) {
	if (!(*this >= d))return true;
	return false;
}
// <=运算符重载
bool  Date::operator <= (const Date& d) {
	if (!(*this > d))return true;
	return false;
}
// !=运算符重载
bool  Date::operator != (const Date& d) {
	if (!(*this == d))return true;
	return false;
}
// 日期-日期 返回天数
int Date::operator-(const Date& d) {
	Date d1 = (*this);
	Date d2 = d;
	if (d1 < d2) {
		Swap(d1, d2);
	}
	int res = 0;
	while (d2 < d1) {
		int t = GetMonthDay(d2._year, d2._month)+1-d2._day;
		if (d2 + t <= d1) {
			d2 += t;
			res += t;
		}
		else {
			int k = d1._day - d2._day;
			d2 += k;
			res += k;
		}
	}
	return res;
}