#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
//
//class OuterClass {
//public:
//
//    OuterClass():num(10){}
//    // 内部类的定义
//    class InnerClass {
//    public:
//        void display() {
//            cout << "Inner class method" << endl;
//            OuterClass d;
//            cout << d.num << " " << endl;
//        }
//    };
//
//    // 外部类的成员函数
//    void outerMethod() {
//        std::cout << "Outer class method" << std::endl;
//
//        // 在外部类的成员函数中创建内部类对象并调用其方法
//        InnerClass innerObj;
//        innerObj.display();
//    }
//private:
//    int num;
//};
//
//
//
//int main() {
//
//    OuterClass d1;
//    d1.outerMethod();
//
//    OuterClass::InnerClass d2;
//    
//	return 0;
//}
//class C {
//public:
//	class A {
//	public:
//		A() :num(10) {
//			C c;
//			cout << c._num << endl;
//		}
//		class B {
//		public:
//			void Print() {
//				A a;
//				C c;
//				cout << c._num << endl;
//				cout << a.num << endl;
//			}
//		private:
//			int _num;
//		};
//	private:
//		int num;
//	};
//
//private:
//	int _num;
//
//};
//
//
//int main() {
//	C::A a;
//	C::A::B b;
//	b.Print();
//	cout << sizeof a << " " << sizeof b << endl;
//
//	return 0;
//}
// 

//匿名对象

class A
{
public:
	A(int a = 0)
		:_a(a)
	{
		cout << "A(int a)" << endl;
	}
	~A()
	{
		cout << "~A()" << endl;
	}
private:
	int _a;
};
class Solution {
public:
	int Sum_Solution(int a,int b) {
		//...
		return a+b;
	}
};
int main()
{
	//A();
	//一般使用
	cout << Solution().Sum_Solution(1, 2) << endl;
	return 0;
}