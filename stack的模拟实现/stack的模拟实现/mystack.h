#define _CRT_SECURE_NO_WARNINGS 1
#include<vector>


template<class T,class Container = std::vector<T> >
class stack {
public:
	//默认构造函数会自动调用成员变量的构造函数
	//默认析构函数会自动调用成员变量的析构函数
	size_t size() {
		return _con.size();
	}
	bool empty() {
		return _con.empty();
	}
	void push(const T& val) {
		_con.push_back(val);
	}
	T top() {
		return _con[_con.size() - 1];
	}
	void pop() {
		_con.pop_back();
	}

	void swap(stack<T>& x) {
		_con.swap(x._con);
	}

private:
	Container _con;
};