#define _CRT_SECURE_NO_WARNINGS 1
#include"mystack.h"
#include"myqueue.h"
#include<iostream>

using namespace std;

void test1() {
	stack<int> st;
	st.push(1);
	st.push(2);
	st.push(3);

	while (!st.empty()) {
		cout << st.top() << endl;
		st.pop();
	}
}

void test2() {
	queue<int> q1;
	q1.push(1);
	q1.push(2);
	q1.push(3);
	q1.push(4);

	while (!q1.empty()) {
		cout << q1.front() << endl;
		q1.pop();
	}
}
int main() {
	test1();
	//test2();
	return 0;
}