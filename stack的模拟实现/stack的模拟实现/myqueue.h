#define _CRT_SECURE_NO_WARNINGS 1
#include<deque>

template<class T, class Container = std::deque<T> >
class queue {
public:
	//默认构造函数会自动调用成员变量的构造函数
	//默认析构函数会自动调用成员变量的析构函数
	size_t size() {
		return _con.size();
	}
	bool empty() {
		return _con.empty();
	}
	void push(const T& val) {
		_con.push_back(val);
	}
	T front() {
		return _con.front();
	}
	T back() {
		return _con.back();
	}
	void pop() {
		_con.pop_front();
	}

	void swap(stack<T>& x) {
		_con.swap(x._con);
	}

private:
	Container _con;
};