#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

class A {
public:
	virtual void add() {
		cout << "A " << endl;
		hobby();
	}

	virtual void hobby() {
		cout << "A hobby" << endl;
	}
};

class B:public A {
public:
	virtual void add() {
		cout << "B add" << endl;
		hobby();
	}

	virtual void hobby() {
		cout << "B hobby" << endl;
	}

	static int num;
};
char* getMe() {
	char p[] = "hello";
	return p;
}
void test() {
	char* str = NULL;
	str = getMe();
	(*str)++;
	printf(str);
	(*str)++;
}
int main() {
	test();
	/*B* b = new B();
	A* a = (A*)b;
	a->add();
	cout << sizeof B << endl;*/

	return 0;
}