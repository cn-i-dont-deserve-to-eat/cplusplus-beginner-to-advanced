﻿#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
//#include <crtdbg.h>
#include<stdio.h>
using namespace std;

//int main() {
//	int a[4] = { 1,2,3,4 };
//	printf("%p------%d\n", &a[0], a[0]);
//	printf("%p------%d\n", &a[1], a[1]);
//	printf("%p------%d\n", &a[2], a[2]);
//	printf("%p------%d\n", &a[3], a[3]);
//	int* t = a + 1;
//	printf("%p------%d\n", t+1, *(t+1));
//	printf("***************************************\n");
//	int a1=0, a2=1, a3=3, a4=4;
//	printf("%p------%d\n", &a1, a1);
//	printf("%p------%d\n", &a2, a2);
//	printf("%p------%d\n", &a3, a3);
//	printf("%p------%d\n", &a4, a4);
//	return 0;
//}

class A
{
public:
	A(int a = 0)
		: _a(a)
		
	{
		b = a;
		cout << "A():" << this << endl;
	}
	~A()
	{
		cout << "~A():" << this << endl;
	}
private:
	int _a;
	double b;
};

class B {
public:
	~B() {
		cout << "~B()" << endl;
	}
};


class C {
public:
	~C() {
		cout << "~C()" << endl;
	}
};



class D {
public:
	~D() {
		cout << "~D()" << endl;
	}
};


void test1() {
	//为对象开辟空间
	A* p1 = (A*)malloc(sizeof(A));
	A* p2 = new A(1);
	free(p1);
	delete p2;
	//cout << "---------------\n";
	//// 为内置类型开辟空间
	//int* p3 = (int*)malloc(sizeof(int)); // C
	//int* p4 = new int;
	//free(p3);
	//delete p4;
	//cout << "---------------\n";
	////对象数组开辟空间
	//A* p5 = (A*)malloc(sizeof(A) * 10);
	//A* p6 = new A[10];
	//free(p5);
	//delete[] p6;
	//cout << "---------------\n";
}
void test2() {
	int* a = new int[4];
	int* b = new int;
	delete[] a;
	delete b;
}
void test3() {
	A* p1 = new A[5];
	delete[] p1;
	//delete p1;错误
	//delete p1;
	
}

C c;

int main()

{

	A* pa = new A();
    
	B b;
    
    static D d;
	delete pa;
	return 0;

}
//
//int main()
//{
//	test3();
//	/*_CrtDumpMemoryLeaks();
//	int* t = new int[10];
//
//    _CrtDumpMemoryLeaks();
//	return 0;*/
