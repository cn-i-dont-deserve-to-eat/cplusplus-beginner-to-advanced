#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include <time.h>
using namespace std;
//
//int& Add(int a, int b)
//{
//	int c = a + b;
//	cout << &c << endl;
//	cout << c << endl;
//	return c;
//}
//int main()
//{
//	int& ret = Add(1, 2);
//	cout << &ret << endl;
//	cout << "Add(1, 2) is :" <<  ret << endl;
//	return 0;
//}

//int* Add(int a, int b)
//{
//	int c = a + b;
//	cout << &c << endl;
//	cout << c << endl;
//	return &c;
//}
//int main()
//{
//	int* ret = Add(1, 2);
//	cout << ret << endl;
//	cout << "Add(1, 2) is :" << *ret << endl;
//	return 0;
//}

//传值、传引用做参数比较
//struct A { 
//	int a[10000];
//};//用作测试的结构体，为了让测试更加明显，我们在里面定义一个空间较大的数组
//
//void TestFunc1(A a) {}
//void TestFunc2(A& a) {}
//void TestRefAndValue()
//{
//	A a;
//	// 以值作为函数参数，传参10000次并记录时间
//	size_t begin1 = clock();//获得当前时间的毫秒值，也就是测试的开始时间
//	for (size_t i = 0; i < 10000; ++i)
//		TestFunc1(a);
//	size_t end1 = clock();//循环结束毫秒值
//
//
//	// 以引用作为函数参数
//	size_t begin2 = clock();
//	for (size_t i = 0; i < 10000; ++i)
//		TestFunc2(a);
//	size_t end2 = clock();
//	// 分别计算两个函数运行相同次数结束后的时间
//	cout << "TestFunc1(A)用值传参测试所需时间:" << end1 - begin1 << endl;
//	cout << "TestFunc2(A&)引用传参测试所需时间:" << end2 - begin2 << endl;
//}
//int main() {
//	TestRefAndValue();
//}



//值和引用的作为返回值类型的性能比较
//struct A { int a[10000]; };
//A a;
//
//// 值返回
//A TestFunc1() { return a; }//
//// 引用返回
//A& TestFunc2() { return a; }
//void TestReturnByRefOrValue()
//{
//	// 以值作为函数的返回值类型
//	size_t begin1 = clock();
//	for (size_t i = 0; i < 100000; ++i)
//		TestFunc1();
//	size_t end1 = clock();
//	// 以引用作为函数的返回值类型
//	size_t begin2 = clock();
//	for (size_t i = 0; i < 100000; ++i)
//		TestFunc2();
//	size_t end2 = clock();
//	// 计算两个函数运算完成之后的时间
//	cout << "TestFunc1测试用 值 做返回值的总时间" << end1 - begin1 << endl;
//	cout << "TestFunc2 测试用 引用 做返回值的总时间" << end2 - begin2 << endl;
//}
//
//int main() {
//	TestReturnByRefOrValue();
//	return 0;
//}
/*int a = 10;
	int& ra = a;
	cout << &ra << " " << &a << endl;*/
int main() {
	/*int a = 10;
	int& ra = a;
	ra = 20;

	int* pa = &a;
	*pa = 20;*/

	int a = 10;
	int& ra = a;
	int* pa = &a;
    

	return 0;
}
