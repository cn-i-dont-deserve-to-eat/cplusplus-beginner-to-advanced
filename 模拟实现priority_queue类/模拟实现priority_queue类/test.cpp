#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<queue>
//#include"my_priority_queue.h"

using namespace std;

//
//
//void test1() {
//	priority_queue<int,vector<int>,Great<int>> q;
//	q.push(7);
//	q.push(6);
//	q.push(5);
//	q.push(9);
//	q.push(8);
//	q.push(7);
//	while (!q.empty()) {
//		cout << q.top() << endl;
//		q.pop();
//	}
//
//}

void test2() {
	//priority_queue<int> q;
	priority_queue<int, vector<int>, greater<int>>q;
	vector<int> A = { 1,3,2,5,4,6,7,9,8 };
	for (auto& it : A) {
		q.push(it);
	}

	while (!q.empty()) {
		cout << q.top() << " ";
		q.pop();
	}


}

class fun {
public:
	int operator()(int& A, int& B) {
		return A + B;
	}

};
void test3() {
	fun f;
	int a = 1;
	int b = 2;
	cout << f(a, b) << endl;

}
class Date
{
public:
	Date(int year = 1900, int month = 1, int day = 1)
		: _year(year)
		, _month(month)
		, _day(day)
	{}

	bool operator<(const Date& d)const
	{
		return (_year < d._year) ||
			(_year == d._year && _month < d._month) ||
			(_year == d._year && _month == d._month && _day < d._day);
	}

	bool operator>(const Date& d)const
	{
		return (_year > d._year) ||
			(_year == d._year && _month > d._month) ||
			(_year == d._year && _month == d._month && _day > d._day);
	}

	friend ostream& operator<<(ostream& _cout, const Date& d)
	{
		_cout << d._year << "-" << d._month << "-" << d._day;
		return _cout;
	}

private:
	int _year;
	int _month;
	int _day;
};

void TestPriorityQueue()
{
	// 大堆，需要用户在自定义类型中提供<的重载
	priority_queue<Date> q1;
	q1.push(Date(2018, 10, 29));
	q1.push(Date(2018, 10, 28));
	q1.push(Date(2018, 10, 30));
	cout << q1.top() << endl;

	// 如果要创建小堆，需要用户提供>的重载
	priority_queue<Date, vector<Date>, greater<Date>> q2;
	q2.push(Date(2018, 10, 29));
	q2.push(Date(2018, 10, 28));
	q2.push(Date(2018, 10, 30));
	cout << q2.top() << endl;
}

int main() {
	//test1();
	//test2();
	test3();
	return 0;
}