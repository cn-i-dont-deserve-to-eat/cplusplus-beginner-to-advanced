#define _CRT_SECURE_NO_WARNINGS 1
#include<list>
#include<iostream>
using namespace std;

int main() {
	list<int> A = { 1,2,3,4,5 };
	list<int>::reverse_iterator it = A.rbegin();
	while (it != A.rend()) {
		cout << *it << endl;
		it++;
	}

	return 0;
}