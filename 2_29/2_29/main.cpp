#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>

using namespace std;

//#include"test.h"
//using namespace std;
//struct structname {
//	//结构体
//};

//人
//class person {
//public:
//	//在类里面定义一个显示信息的成员函数
//	void show() {
//		cout << age << " " << name << " " << sex << endl;
//	}
//
//	//成员变量
//	int age;//年龄
//	const char* name;//姓名
//	const char* sex;//性别
//};

//日期
//class Date{
//public:
//	int _y;
//	void print_y() {
//		cout << _y << endl;
//	}
//private:
//	int _m;
//	void print_m() {
//		cout << _m << endl;
//	}
//};

//struct a {
//  
//	char _gender[3];
//	//char b;
//    int _age;
//	char _name[20];
//	
//}a1;
//



// 类中既有成员变量，又有成员函数
class A1 {
public:
	void f1() {}
private:
	int _a;
};
// 类中仅有成员函数
class A2 {
public:
	void f2() {}
};
// 类中什么都没有---空类
class A3
{};

struct A4 {
	
};


class Person
{
public:
	void PrintPersonInfo();
private:
	//char _name[20];
	char _gender[3];
	int _age;
	char b;
};
// 这里需要指定PrintPersonInfo是属于Person这个类域
void Person::PrintPersonInfo()
{//
	//cout << _name << " " << _gender << " " << _age << endl;
}

int main() {
    Person p1;
	Person p2;
	Person p3;

	cout << sizeof A1 << endl;
	cout << sizeof A2 << endl;
	cout << sizeof A3 << endl;


	//cout << sizeof A4 << endl;
	
	//printf("%d\n", sizeof a);
	//std::cout<<sizeof Person;

	//Date d;
	//d._y = 12;
	//d.print_y();
	//d._m = 20;
	//d.print_m();

	/*person s1;
	s1.age = 18;
	s1.name = "zhangshan";
	s1.sex = "man";
	s1.show();*/


	return 0;
}