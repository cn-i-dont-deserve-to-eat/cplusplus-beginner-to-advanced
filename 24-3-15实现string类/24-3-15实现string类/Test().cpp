#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include"string.h"
using namespace bit;
using std::cout;
using std::cin;
using std::endl;
void test1() {
	string A = "assddfff";
	std::cout << (A.find("kkks")) << std::endl;
}

void test2() {
	string A = "aaaaaa";
	string B(A);
     
	A += "kkkkk";
	B.push_back('c');

	std::cout << (A.c_str()) << " " << B.c_str() << std::endl;
	for (auto it : A) {
		std::cout << it << " ";
	}
}

void test3() {
	string A = "aaaaa";
	A.insert(5, 's');
	std::cout << A.capacity() << std::endl;
	for (auto it : A) {
		std::cout << it << " ";
	}
	std::cout << (A.c_str()) << std::endl;
	//std::cout << std::endl;

	A.insert(6, "hhhfrrggf");
	for (auto it : A) {
		std::cout << it << " ";
	}
	std::cout << (A.c_str()) << std::endl;
}
void test4() {
	string A = "aaaaa";
	A.erase(2, 3);
	std::cout << A << std::endl;
}

void test5() {
	string A;
	string B;
	cin >> A>>B;
	cout << A << " " << B << endl;

	getline(cin, A);
	cout << A;
}
void test6() {
	string A = "123";
	string B;
	B = A;
}
int main() {
	//test1();//find
	//test2();//  += 
	//test3();//insert()
	//test4();
	//test5();//cin/cout
	test6();
	return 0;
}