#define _CRT_SECURE_NO_WARNINGS 1
#include"string.h"

bit::string::string(const char* str) :_size(strlen(str)){
	_str = new char[_size+1];
	strcpy(_str, str);
	_capacity = _size;
}

bit::string::string(const string& s) {
	_str = new char[s._capacity + 1];
	strcpy(_str, s._str);
	_size = s._size;
	_capacity = s._capacity;
}

bit::string& bit::string::operator=(const string& s) {
	char* temp = new char[s._capacity+1];
	strcpy(temp, s._str);

	delete[] _str;
	_capacity = s._capacity;
	_size = s._size;
	_str = temp;
	return *this;
}

bit::string::~string() {
	delete[] _str;
	_capacity = 0;
	_size = 0;
}

// iterator
//迭代器
bit::string::iterator bit::string::begin() {
	return _str;
}
bit::string::iterator bit::string::end() {
	return _str + _size;
}

const bit::string::iterator bit::string::begin()const {
	return _str;
}
const bit::string::iterator bit::string::end() const{
	return _str + _size;
}


/////////////////////////////////////////////////////////////

// 增加字符函数

void bit::string::push_back(char c) {
	if (_size == _capacity) {
		reserve(_capacity ==0?4:_capacity*2);
	}
	_str[_size++] = c;
	_str[_size] = '\0';
}

bit::string& bit::string::operator+=(char c) {
	(*this).push_back(c);
	return *this;
}

void bit::string::append(const char* str) {
	size_t n = strlen(str);
	for (int i = 0; i < n; i++)(*this).push_back(str[i]);
}

bit::string& bit::string::operator+=(const char* str) {
	append(str);
	return *(this);
}

void bit::string::clear() {
	_size = 0;
	_str[_size] = '\0';
}

void bit::string::swap(bit::string& s) {
	std::swap(s._str, _str);
	std::swap(s._size, _size);
	std::swap(s._capacity, _capacity);
}

const char* bit::string::c_str()const {
	return _str;
}



/////////////////////////////////////////////////////////////

// capacity
//容量修改有关函数重载
size_t bit::string::size()const {
	return _size;
}

size_t bit::string::capacity()const {
	return _capacity;
}

bool bit::string::empty()const {
	return _size == 0;
}

void bit::string::resize(size_t n, char c ) {
	if (n <=_size) {
		_str[n] = '\0';
		_size = n;
	}
	else {
		reserve(n);
		for (size_t i = _size; i < n; i++) {
			_str[i] = c;
		}
		_str[n] = '\0';
		_capacity = n;
		_size = n;
	}
}

void bit::string::reserve(size_t n) {
	if (n > _capacity) {
		char* temp = new char[n + 1];
		strcpy(temp, _str);
		
		delete[] _str;
		_str = temp;
		_str[_size] = '\0';
		_capacity = n;
	}
}

// access
//下标访问

char& bit::string::operator[](size_t index) {
	assert(index >= 0 && index < _size);
	return _str[index];
}

const char& bit::string::operator[](size_t index)const {
	assert(index >= 0 && index < _size);
	return _str[index];
}


//relational operators
//比较操作符重载

bool bit::string::operator<(const bit::string& s) {
	int f = strcmp(_str, s._str);
	return f< 0;
}
bool bit::string::operator<=(const bit::string& s) {
	int f = strcmp(_str, s._str);
	return f <= 0;
}

bool bit::string::operator>(const bit::string& s) {
	int f = strcmp(_str, s._str);
	return f >0 ;
}


bool bit::string::operator>=(const bit::string& s) {
	int f = strcmp(_str, s._str);
	return f >= 0;
}

bool bit::string::operator==(const bit::string& s) {
	int f = strcmp(_str, s._str);
	return f == 0;
}


bool bit::string::operator!=(const bit::string& s) {
	int f = strcmp(_str, s._str);
	return f != 0;
}


// 返回c在string中第一次出现的位置

int  bit::string::find(char c, size_t pos ) const {
	assert(pos >= 0 && pos < _size);
	for (size_t i = pos; i < _size; i++) {
		if (_str[i] == c)return i;
	}
	return -1;
}

// 返回子串s在string中第一次出现的位置

int bit::string::find(const char* s, size_t pos ) const {
	assert(pos >= 0 && pos < _size);
	const char* t= strstr(_str + pos, s);
	if (t == NULL)return -1;
	return t - _str;
}

// 在pos位置上插入字符c/字符串str，并返回该字符的位置

bit::string& bit::string::insert(size_t pos, char c) {
	assert(pos <=_size);
	reserve(_capacity + 1);
	//_str[_size] = '\0';
	size_t end = _size+1;
	while (end >= pos) {
		_str[end] = _str[end-1];
		end--;
	}
	_str[pos] = c;
	_size++;
	return *this;
}
//
bit::string & bit::string::insert(size_t pos, const char* str) {
	assert(pos <= _size);
	size_t len = strlen(str);
	reserve(_capacity + len);
	size_t end = _size + len;
	while (end >= pos+len) {
		_str[end] = _str[end - len];
		end--;
	}
	strncpy(_str + pos, str, len);
	//size_t cnt = 0;
	/*for (size_t i = pos; i < pos + len; i++) {
		_str[i] = str[cnt++];
	}*/
	_size += len;
	_str[_size] = '\0';
	return *this;
}
//
//
//
//// 删除pos位置上的元素，并返回该元素的下一个位置
//
bit::string& bit::string::erase(size_t pos, size_t len) {
	assert(pos < _size);
	if (len >= _size - pos)len = _size - pos;
	size_t t = pos + len;
	strncpy(_str + pos, _str + t,len);
	_size--;
	_str[_size] = '\0';
	return *this;
}

std::ostream& bit::operator<<(std::ostream& _cout, const bit::string& s) {
	for (auto it : s) {
		_cout << it;
	}
	return _cout;
}

//std::istream& bit::operator>>(std::istream& _cin, bit::string& s) {
//	s.clear();
//	char ch;
//	ch = _cin.get();
//	s.reserve(128);
//	while (ch != '\n' && ch != ' ') {
//		s += ch;
//		ch = _cin.get();
//	}
//	return _cin;
//}
std::istream& bit::operator>>(std::istream& _cin, bit::string& s) {
	s.clear();
	char ch;
	ch = _cin.get();
	char buff[128];
	size_t cnt = 0;
	while (ch != '\n' && ch != ' ') {
		buff[cnt++] = ch;
		if (cnt == 127) {
			s += buff;
			cnt = 0;
		}
		ch = _cin.get();
	}
	if (cnt != 0) {
		buff[cnt] = '\0';
		s += buff;
		//s[(s.size())] = '\0';
		cnt = 0;
	}
	
	return _cin;
}

std::istream& bit::getline(std::istream& _cin, bit::string& s) {
	s.clear();
	char ch;
	ch = _cin.get();
	char buff[128];
	size_t cnt = 0;
	while (ch != '\n' ) {
		buff[cnt++] = ch;
		if (cnt == 127) {
			s += buff;
			cnt = 0;
		}
		ch = _cin.get();
	}
	if (cnt != 0) {
		buff[cnt] = '\0';
		s += buff;
		//s[(s.size())] = '\0';
		cnt = 0;
	}

	return _cin;
}

