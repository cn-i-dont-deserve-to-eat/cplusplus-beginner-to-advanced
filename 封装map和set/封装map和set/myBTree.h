#pragma once

#include<vector>
#include<iostream>
#include<algorithm>
using namespace std;



enum Colour {
	RED,
	BLACK
};

template<class T>
struct RBTreeNode {
	RBTreeNode<T>* _parent;
	RBTreeNode<T>* _left;
	RBTreeNode<T>* _right;
	T _date;

	Colour _col;//颜色

	RBTreeNode(const T& date)
		:_date(date)
		, _parent(nullptr)
		, _left(nullptr)
		, _right(nullptr)
		, _col(RED)
	{

	}

};

template<class T,class Ref,class Ptr>
struct __RBTreeIterator {
	typedef RBTreeNode<T> Node;
	typedef Node* pNode;
	typedef __RBTreeIterator<T, Ref, Ptr> self;
	pNode _node;

	__RBTreeIterator(pNode node)
		:_node(node)
	{}

	Ref operator*() {
		return _node->_date;
	}

	Ptr operator->() {
		return &(_node->_date);
	}

	self& operator++() {
		if (_node->_right) {
			pNode rightmin = _node->_right;
			while (rightmin->_left)rightmin = rightmin->_left;
			_node = rightmin;
		}
		else {
			pNode parent = _node->_parent;
			pNode cur = _node;
			while (parent && parent->_left != cur) {
				cur = parent;
				parent = parent->_parent;
			}
			_node = parent;
			
		}
		return  *this;
	}
	bool operator!=(const self& t) {
		return t._node != this->_node;
	}
};

template<class K, class T,class KeyOft>
class RBTree {
public:
	typedef RBTreeNode<T> Node;
	typedef Node* pNode;
	typedef __RBTreeIterator<T, T&, T*> Iterator;
	typedef __RBTreeIterator<T, const T&, const T*> const_Iterator;

public:
	//迭代器操作
	Iterator begin() {
		pNode cur = _root;
		while (cur&&cur->_left)cur = cur->_left;
		return Iterator(cur);
	}
	const_Iterator begin()const {
		pNode cur = _root;
		while (cur && cur->_left)cur = cur->_left;
		return Iterator(cur);
	}

	Iterator end() {
		return  Iterator(nullptr);
	}
	const_Iterator end() const{
		return  Iterator(nullptr);
	}

	RBTree<K, T, KeyOft>& operator=(RBTree<K,T,KeyOft> t) {//赋值
		swap(t._root, this->_root);
		return *this;
	}
	RBTree() = default;//强制生成默认构造函数
	RBTree(const RBTree<K,T,KeyOft>& t) {//拷贝构造
		_root = Copy(t._root);
	}

	~RBTree() {
		Destroy(_root);
		_root = nullptr;
	}
	//插入新节点
	pair<Iterator,bool> Insert(const T& date) {
		if (_root == nullptr) {
			_root = new Node(date);
			_root->_col = BLACK;
			return std::make_pair(Iterator(_root), true);
		}
		//找到一个合适的插入位置
		pNode cur = _root;
		pNode parent = nullptr;
		KeyOft kft;
		while (cur) {
			if (kft(cur->_date)>kft(date)) {
				parent = cur;
				cur = cur->_left;
			}
			else if (kft(cur->_date) < kft(date)) {
				parent = cur;
				cur = cur->_right;
			}
			else {
				return std::make_pair(Iterator(cur),false);
			}
		}
		//此时cur为nullptr
		cur = new Node(date);
		pNode newnode = cur;
		if (kft(cur->_date)<kft(parent->_date)) {//插入节点
			parent->_left = cur;
			cur->_parent = parent;
		}
		else {
			parent->_right = cur;
			cur->_parent = parent;
		}

		//插入之后需要调节颜色平衡

		while (parent && parent->_col == RED) {
			//找叔叔节点
			pNode grandparent = parent->_parent;//祖父节点一定不为空，因为父节点为红色
			if (parent == grandparent->_left) {//如果叔叔在右边
				pNode uncle = grandparent->_right;
				//叔叔存在且为红，直接都变成黑色
				if (uncle && uncle->_col == RED) {
					parent->_col = BLACK;
					uncle->_col = BLACK;
					grandparent->_col = RED;
					//继续往上处理
					cur = grandparent;//cur往上跳两个节点
					parent = cur->_parent;
				}//叔叔节点不存在或者为黑色
				else {
					if (cur == parent->_left) {
						RotateR(grandparent);
						parent->_col = BLACK;
						grandparent->_col = RED;
					}
					else if (cur == parent->_right) {
						RotateL(parent);
						RotateR(grandparent);
						cur->_col = BLACK;
						grandparent->_col = RED;
					}
					break;
				}
			}
			//如果叔叔在左边
			if (parent == grandparent->_right) {
				pNode uncle = grandparent->_left;
				//叔叔存在且为红，直接都变成黑色
				if (uncle && uncle->_col == RED) {
					parent->_col = BLACK;
					uncle->_col = BLACK;

					//继续往上处理
					grandparent->_col = RED;
					cur = grandparent;//cur往上跳两个节点
					parent = cur->_parent;
				}//叔叔节点不存在或者为黑色
				else {
					if (cur == parent->_left) {
						RotateR(parent);
						RotateL(grandparent);
						cur->_col = BLACK;
						grandparent->_col = RED;
					}
					else if (cur == parent->_right) {
						RotateL(grandparent);
						parent->_col = BLACK;
						grandparent->_col = RED;
					}
					break;
				}
			}
		}
		_root->_col = BLACK;
		return std::make_pair(Iterator(newnode),true);
	}

	//查找某个节点
	Iterator find(const K& key) {
		pNode cur = _root;
		KeyOft kft;
		while (cur) {
			if (key < kft(cur->_date)) {
				cur = cur->_left;
			}
			else if (key > kft(cur->_date)) {
				cur = cur->_right;
			}
			else {
				return Iterator(cur);
			}
		}
		return Iterator(nullptr);
	}

	void RotateR(Node* parent)//右旋
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;

		parent->_left = subLR;
		if (subLR)
			subLR->_parent = parent;

		subL->_right = parent;

		Node* ppNode = parent->_parent;
		parent->_parent = subL;

		if (parent == _root)
		{
			_root = subL;
			_root->_parent = nullptr;
		}
		else
		{
			if (ppNode->_left == parent)
			{
				ppNode->_left = subL;
			}
			else
			{
				ppNode->_right = subL;
			}

			subL->_parent = ppNode;
		}
	}

	void RotateL(Node* parent)//左旋
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;

		parent->_right = subRL;
		if (subRL)
			subRL->_parent = parent;

		subR->_left = parent;
		Node* ppNode = parent->_parent;

		parent->_parent = subR;

		if (parent == _root)
		{
			_root = subR;
			_root->_parent = nullptr;
		}
		else
		{
			if (ppNode->_right == parent)
			{
				ppNode->_right = subR;
			}
			else
			{
				ppNode->_left = subR;
			}
			subR->_parent = ppNode;
		}
	}
	bool IsBalance() {
		if (_root->_col == RED)return false;

		int targetnum = 0;
		pNode cur = _root;
		while (cur) {
			if (cur->_col == BLACK)targetnum++;
			cur = cur->_left;
		}
		return _Check(_root, targetnum, 0);
	}
	void InOrder() {
		_InOrder(_root);
	}
private:
	//递归深拷贝红黑树的每个节点
	pNode Copy(pNode root) {
		if (root == nullptr)return nullptr;

		pNode newnode = new Node(root->_date);
		newnode->_col = root->_col;
		pNode left=Copy(root->_left);
		pNode right=Copy(root->_right);

		newnode->_left = left;
		newnode->_right = right;
		if(left)left->_parent = newnode;
		if(right)right->_parent = newnode;
		return newnode;
	}
	//递归析构节点
	void Destroy(pNode root) {
		if (root == nullptr) {
			return ;
		}
		Destroy(root->_left);
		Destroy(root->_right);
		delete root;
		root = nullptr;
	}
	//查找当前红黑树是否平衡
	bool _Check(pNode root, int targetnum, int blacknum) {
		if (root == nullptr) {
			if (blacknum != targetnum) {//路径的黑色节点数不相等
				return false;
			}
			else return true;
		}
		if (root->_left && root->_col == RED && root->_left->_col == RED)return false;
		if (root->_right && root->_col == RED && root->_right->_col == RED)return false;

		if (root->_col == BLACK)blacknum++;

		return _Check(root->_left, targetnum, blacknum) && _Check(root->_right, targetnum, blacknum);
	}
	//中序遍历红黑树
	void _InOrder(pNode root) {
		KeyOft kft;
		if (root == nullptr) {
			return;
		}
		_InOrder(root->_left);
		cout << kft(root->_date)<< endl;
		_InOrder(root->_right);
	}

	pNode _root = nullptr;

};
