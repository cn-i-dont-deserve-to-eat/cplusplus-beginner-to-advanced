#pragma
#include"myBTree.h"

namespace bit {
	template<class K>
	class set {

		struct MapKeyOft {
			const K& operator()(const K& k) {
				return k;
			}
		};
	public:
		typedef typename RBTree<K, K, MapKeyOft>::Iterator iterator;
		typedef typename RBTree<K, const K, MapKeyOft>::const_Iterator const_iterator;

		iterator begin() {
			return _t.begin();
		}

		iterator end() {
			return _t.end();
		}

		const_iterator begin() const {
			return _t.begin();
		}

		const_iterator cend() const {
			return _t.end();
		}
		iterator find(const K& key) {
			return _t.find(key);
		}
		pair<iterator, bool> insert(const K& k) {
			return _t.Insert(k);
		}


	private:
		RBTree<K, K, MapKeyOft> _t;
	};

}
