#define _CRT_SECURE_NO_WARNINGS 1
#include"myBTree.h"
#include"myMap.h"
#include"mySet.h"
#include<iostream>
using namespace bit;
void test_map1()
{
	map<string, int> m;
	m.insert({ "b",1 });
	m.insert({ "c",1 });
	m.insert({ "a",1 });
	m.insert({ "d",3 });

	map<string, int>::iterator it = m.begin();
	while (it != m.end())
	{
		//it->first += 'x';
		it->second += 1;

		//cout << it.operator->()->first << ":" << it->second << endl;
		cout << it->first << ":" << it->second << endl;
		++it;
	}
	cout << endl;
}

void test1() {
	map<int, int> mp;
	mp.insert({ 1,1 });
	
}
void test_map2()
{
	string arr[] = { "a", "d", "b", "z", "h", "h", "h",
"a", "z", "a", "c","d","g", "a","g" };
	map<string, int> countMap;
	for (auto& e : arr)
	{
		countMap[e]++;
	}

	for (auto& kv : countMap)
	{
		cout << kv.first << ":" << kv.second << endl;
	}
	cout << endl;
}
void test_set2()
{
	string arr[] = { "a", "d", "b", "z", "h", "h", "h",
"a", "z", "a", "c","d","g", "a","g" };
	set<string> countMap;
	for ( auto& e : arr)
	{
		countMap.insert(e);
	}

	for (auto& kv : countMap)
	{
		cout << kv << endl;
	}
	cout << endl;
}


int main() {
	//test1();
	//test_map1();
	test_set2();
	return 0;
}