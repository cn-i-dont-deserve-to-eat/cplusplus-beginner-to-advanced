#pragma
#include"myBTree.h"

namespace bit {
	template<class K, class V>
	class map {

		struct MapKeyOft {
			const K& operator()(const pair<K, V>& kv) {
				return kv.first;
			}
		};
	public:
		typedef typename RBTree<K, pair<const K, V>, MapKeyOft>::Iterator iterator;
		typedef typename RBTree<K, const K,  MapKeyOft>::const_Iterator const_iterator;

		iterator begin() {
			return _t.begin();
		}

		iterator end() {
			return _t.end();
		}

		const_iterator begin() const {
			return _t.begin();
		}

		const_iterator cend() const {
			return _t.end();
		}
		iterator find(const K& key) {
			return _t.find(key);
		}
		pair<iterator, bool> insert(const pair<K, V>& kv) {
			return _t.Insert(kv);
		}

		V& operator[](const K& key) {
			pair<iterator, bool> res = _t.Insert(make_pair(key, V()));
			return res.first->second;
		}


	private:
		RBTree<K, pair<const K, V>, MapKeyOft> _t;
	};

}
