#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<algorithm>
using namespace std;

//
//class Person
//{
//public:
//	void Print()
//	{
//		cout << "name:" << _name << endl;
//		cout << "age:" << _age << endl;
//	}
//protected:
//	string _name = "peter"; // 姓名
//	int _age = 18; // 年龄
//};
//
//class Student :public Person {
//protected:
//	int _stuid;
//};
//
//
//
//int main() {
//	Student s;
//	Teacher t;
//	s.Print();
//	t.Print();
//	return 0;
//}


//class person {
//public:
//	void pub_mem() {//public成员
//		cout << "pub_mem" << endl;
//	}
//protected://protected成员
//	void prot_mem() {
//		cout << "prot_mem" << endl;
//	}
//private://private成员
//	void priv_mem() {
//		cout << "priv_mem" << endl;
//	}
//};
//
//class student:public person {
//public:
//	void fun() {
//		pub_mem();
//		prot_mem();
//		//priv_mem();
//	}
//
//};
//class Person
//{
//protected:
//	string _name; // 姓名
//	string _sex; // 性别
//	int _age; // 年龄
//};
//class Student : public Person
//{
//public:
//	int _No; // 学号
//};
//
//
//void test() {
//	Student sobj;
//	Person pobj = sobj;
//	Person* pp = &sobj;
//	Person& rp = sobj;
//
//}
//int main() {
//	test();
//	return 0;
//}
// 
// 
// Student的_num和Person的_num构成隐藏关系，可以看出这样代码虽然能跑，但是非常容易混淆
class Person
{
public:
	Person(string name, int age) 
       :_name(name)
		,_age(age)
	{
		cout << "person 构造" << endl;
	} 

	~Person() {
		cout << "person 析构" << endl;
	}
private:
	string _name ; 
	int _age; 
};
class Student : public Person
{
public:
	Student(string name="zhangsan", int age=18, int num=110)
		:Person(name, age)
		,_num(num)
	{
		cout << "student 构造" << endl;
	}

	~Student() {
		Person::~Person();
		cout << "student 析构" << endl;
	}
private:
	int _num;
};
void Test()
{
	Student s1;
	
};




//class A {
//	friend class B;
//private:
//	int _a;
//};
//
//class C:public A {
//protected:
//	int num;
//private:
//	int _c;
//};
//
//class B {
//public:
//	void fun() {
//		C c;
//		//cout << c.num;
//	}
//private:
//	int b;
//};

class A {
public:
	A() {
		num++;
	}
	void print() {
		cout << num << endl;
	}
private:
	static int num;
};

class B :A {
public:
	B() {

	}
};
int A::num = 0;
int main() {
	A a;
	B b1;
	B b2;
	B b3;
	a.print();
	return 0;
}