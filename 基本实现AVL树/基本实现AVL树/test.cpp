#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include"myAVLTree.h"
using namespace std;
using namespace k_val;

int main() {
	int a[10] = { 1,7,8,3,4,9,10,2,6,5 };
	AVLTree<int, int> avl;
	for (int i = 0; i < 10; i++) {
		avl.Insert({ a[i],a[i] });
 	}

	avl.InOrder();

	return 0;
}