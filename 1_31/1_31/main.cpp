#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

void Print(int x) {
	cout << x << "是一个整数" << endl;
}

void Print(double x) {
	cout << x << "是一个浮点数" << endl;
}

int add(int a,int b,int c) {//三数相加
	return a + b + c;
}

int add(int a,int b) {//两数相加
	return a + b;
}

void f(char a, int b) {
	cout << "f(char,int)" << endl;
}

void f(int b, char a) {
	cout << "f(int,char)" << endl;
}

int fun() {

}
void fun(int a) {

}
int main() {
	//参数类型构成重载
	Print(3);
	Print(3.4);

	//参数个数构成重载
	printf("%d\n", add(1, 2));
	printf("%d\n", add(1, 2, 3));
	
	//参数顺序构成函数重载
	f('a', 1);
	f(1, 'a');

	return 0;
}