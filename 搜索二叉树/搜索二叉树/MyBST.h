#include<iostream>
#include<algorithm>
#include<assert.h>

namespace k {

	template<class T>
	class BSTNode {
	public:
		BSTNode()
			:_val(0)
		{
			left = nullptr;
			right = nullptr;
		}

		BSTNode(T val)
			:_val(val)
		{
			left = nullptr;
			right = nullptr;
		}
		T _val;
		BSTNode<T>* left;
		BSTNode<T>* right;
	};


	template<class T>
	class BSTree {

		typedef BSTNode<T> Node;
		typedef Node* pNode;
	public:
		BSTree()
			:_root(nullptr)
		{

		}

		//删除
		bool Erase(const T& val) {
			if (_root == nullptr)return false;
			if (!Find(val))return false;
			pNode cur = _root;
			pNode pa = _root;
			while (cur) {
				if (cur->_val < val) {
					pa = cur;
					cur = cur->right;
				}
				else if (cur->_val > val) {
					pa = cur;
					cur = cur->left;
				}
				else break;
			}

			//被删除的节点孩子有空
			if (cur->left == nullptr) {
				if (cur == _root) {//如果被删除的节点是头节点
					pNode t = _root;
					_root = cur->right;
					delete t;
				}
				else if (pa->_val < cur->_val) {
					pa->right = cur->right;
					delete cur;
				}
				else {
					pa->left = cur->right;
					delete cur;
				}

			}
			else if (cur->right == nullptr) {
				if (cur == _root) {//如果被删除的节点是头节点
					pNode t = _root;
					_root = cur->left;
					delete t;
				}
				else if (pa->_val < cur->_val) {
					pa->right = cur->left;
					delete cur;
				}
				else {
					pa->left = cur->left;
					delete cur;
				}

			}//cur的两个孩子节点都不为空
			else {
				pNode ffa = cur;
				pNode minright = cur->right;
				while (minright->left) {
					ffa = minright;
					minright = minright->left;
				}

				ffa->left = minright->right;
				std::swap(minright->_val, cur->_val);
				delete minright;
			}


		}


		//查找
		bool Find(const T& val) {
			assert(_root != nullptr);
			pNode cur = _root;
			while (cur) {
				if (cur->_val < val) {
					cur = cur->right;
				}
				else if (cur->_val > val) {
					cur = cur->left;
				}
				else return true;

			}
			return false;
		}

		//插入
		bool Insert(const T& val) {
			if (_root == nullptr) {
				_root = new Node(val);
				return true;
			}

			pNode cur = _root;
			pNode pa = nullptr;
			while (cur) {
				if (cur->_val < val) {
					pa = cur;
					cur = cur->right;
				}
				else if (cur->_val > val) {
					pa = cur;
					cur = cur->left;
				}
				else return false;//已经存在无需插入

			}
			if (pa->_val > val) {
				pa->left = new Node(val);
			}
			else {
				pa->right = new Node(val);
			}
			return true;
		}
		//遍历
		void InOrder() {
			_InOrder(_root);
			std::cout << std::endl;
		}
	private:
		void _InOrder(pNode root) {
			if (root == nullptr)return;
			_InOrder(root->left);
			std::cout << root->_val << " ";
			_InOrder(root->right);
		}
		pNode _root;
	};
}


namespace k_val {

	template<class K,class V>
	class BSTNode {
	public:
		BSTNode()
			:_key(0)
			,_val(0)
		{
			left = nullptr;
			right = nullptr;
		}

		BSTNode(const K& key,const V& val)
			:_key(key)
			,_val(val)
		{
			left = nullptr;
			right = nullptr;
		}
		K _key;
		V _val;
		BSTNode<K,V>* left;
		BSTNode<K,V>* right;
	};


	template<class K,class V>
	class BSTree {

		typedef BSTNode<K,V> Node;
		typedef Node* pNode;
	public:
		BSTree()
			:_root(nullptr)
		{

		}

		//删除
		bool Erase(const K& key) {
			if (_root == nullptr)return false;
			if (!Find(key))return false;
			pNode cur = _root;
			pNode pa = _root;
			while (cur) {
				if (cur->_key < key) {
					pa = cur;
					cur = cur->right;
				}
				else if (cur->_key > key) {
					pa = cur;
					cur = cur->left;
				}
				else break;
			}

			//被删除的节点孩子有空
			if (cur->left == nullptr) {
				if (cur == _root) {//如果被删除的节点是头节点
					pNode t = _root;
					_root = cur->right;
					delete t;
				}
				else if (pa->_key < cur->_key) {
					pa->right = cur->right;
					delete cur;
				}
				else {
					pa->left = cur->right;
					delete cur;
				}

			}
			else if (cur->right == nullptr) {
				if (cur == _root) {//如果被删除的节点是头节点
					pNode t = _root;
					_root = cur->left;
					delete t;
				}
				else if (pa->_key < cur->_key) {
					pa->right = cur->left;
					delete cur;
				}
				else {
					pa->left = cur->left;
					delete cur;
				}

			}//cur的两个孩子节点都不为空
			else {
				pNode ffa = cur;
				pNode minright = cur->right;
				while (minright->left) {
					ffa = minright;
					minright = minright->left;
				}

				ffa->left = minright->right;
				std::swap(minright->_key, cur->_key);
				delete minright;
			}


		}


		//查找
		pNode Find(const K& key) {
			assert(_root != nullptr);
			pNode cur = _root;
			while (cur) {
				if (cur->_key < key) {
					cur = cur->right;
				}
				else if (cur->_key > key) {
					cur = cur->left;
				}
				else return cur;

			}
			return nullptr;
		}

		//插入
		bool Insert(const K& key,const V& val) {
			if (_root == nullptr) {
				_root = new Node(key,val);
				return true;
			}

			pNode cur = _root;
			pNode pa = nullptr;
			while (cur) {
				if (cur->_key < key) {
					pa = cur;
					cur = cur->right;
				}
				else if (cur->_key > key) {
					pa = cur;
					cur = cur->left;
				}
				else return false;//已经存在无需插入

			}
			if (pa->_key > key) {
				pa->left = new Node(key,val);
			}
			else {
				pa->right = new Node(key,val);
			}
			return true;
		}
		//遍历
		void InOrder() {
			_InOrder(_root);
			std::cout << std::endl;
		}
	private:
		void _InOrder(pNode root) {
			if (root == nullptr)return;
			_InOrder(root->left);
			std::cout << root->_val << " ";
			_InOrder(root->right);
		}
		pNode _root;
	};
}