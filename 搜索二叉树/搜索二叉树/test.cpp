#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<algorithm>
#include"MyBST.h"

using std::cout;
using std::cin;
using std::endl;
using std::string;

void test1() {
	k::BSTree<int> b;
	b.Insert(5);
	b.Insert(9);
	b.Insert(4);
	b.Insert(10);
	b.Insert(6);


	b.InOrder();
	b.Erase(5);
	b.InOrder();
	b.Erase(9);
	b.InOrder();
	b.Erase(4);
	b.InOrder();
	b.Erase(10);
	b.InOrder();
	b.Erase(6);
	b.InOrder();
	//cout << b.Find(6) << endl;

}

void test2() {
	k_val::BSTree<string, string> b;
	b.Insert("ddd", "444");
	b.Insert("aaa", "111");
    b.Insert("eee", "555");
	b.Insert("bbb", "222");
	b.Insert("ccc", "333");
	b.InOrder();
	string str;
	while (cin >> str) {
		auto t = b.Find(str);
		if (t == nullptr) {
			cout << "û���ҵ�" << endl;
		}
		else  {
			cout << t->_val << endl;
		}
	}

}


int main() {
	//test1();
	test2();
	return 0;
}