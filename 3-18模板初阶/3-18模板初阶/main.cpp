#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

//代码举例

//void Swap(int& left, int& right)
//{
//	int temp = left;
//	left = right;
//	right = temp;
//}
//
//void Swap(double& left, double& right)
//{
//	double temp = left;
//	left = right;
//	right = temp;
//}
//
//void Swap(char& left, char& right)
//{
//	char temp = left;
//	left = right;
//	right = temp;
//}
//
//void test1() {
//	int a1 = 1, a2 = 2;
//	Swap(a1, a2);
//
//	double b1 = 1, b2 = 2;
//	Swap(b1, b2);
//
//	char c1 = 'a', c2 = 'b';
//	Swap(c1, c2);
//}
// void test2() {
//int a1 = 1, a2 = 2;
//double b1 = 1.0, b2 = 2.0;
//char c1 = 0, c2 = 1;
//Swap<int>(a1, a2);
////Swap<int>((int)c1, a1);
//Swap<double>(b1, b2);
//cout << a1 << " " << a2 << endl;
//cout << b1 << " " << b2 << endl;
//}
//非模板函数
void Swap(int& left, int& right)
{
	int temp = left;
	left = right;
	right = temp;
}

template<class T>
void Swap(T& left, T& right) {
	T temp = left;
	left = right;
	right = temp;
}


void test3() {
	int a = 1, b = 2;
	Swap(a, b);//优先调用非模板函数,编译器不需要特化

	Swap<int>(a, b);//只调用模板函数实例化
	cout << a << " " << b << endl;
}
int main() {
	//test2();
	test3();
	return 0;
}