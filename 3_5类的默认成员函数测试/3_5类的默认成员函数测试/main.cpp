﻿#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
//
//class Date
//{
//public:
//	Date(int year = 1900, int month = 1, int day = 1)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//		//arr = new(int[10]);
//	}
//	// Date(const Date& d)   // 正确写法
//	Date( const Date& d) // 错误写法：编译报错，会引发无穷递归
//	{
//		_year = d._year;
//		_month = d._month;
//		_day = d._day;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//
//	//int* arr;
//};
//int main()
//{
//	Date d1;
//	Date d2(d1);
//	return 0;
//}
//class A
//{
//public:
//	A()
//	{
//	   arr = new(int[10]);
//	}
//	~A() {
//		delete[] arr;
//	}
//private:
//	int* arr;
//};
//int main()
//{
//	A a1;
//	A a2(a1);
//	return 0;
//}

//构造函数、析构函数调用顺序
//class Date
//{
//public:
//	Date(int year, int minute, int day)
//	{
//		cout << "Date(int,int,int):" << this << endl;
//	}
//	Date(const Date& d)
//	{
//		cout << "Date(const Date& d):" << this << endl;
//	}
//	~Date()
//	{
//		cout << "~Date():" << this << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//Date Test(Date d)
//{
//	Date temp(d);
//	return temp;
//}
//
//int main()
//{
//	Date d1(2022, 1, 13);
//	Test(d1);
//	return 0;	
//}

// 全局的operator==
//class Date
//{
//public:
//	Date(int year = 1900, int month = 1, int day = 1)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	 bool operator==(const Date& d2)
//	{
//		return _year == d2._year
//			&& _month == d2._month
//			&& _day == d2._day;
//	}
//
//	
//	int _year;
//	int _month;
//	int _day;
//};
//
//Date& operator=(Date& d1,const Date& d2) {
//	if (&d1 != &d2) {//如果参数是对象本身，那就不用继续赋值
//		d1._year = d2._year;
//		d1._month = d2._month;
//		d1._day = d2._day;
//	}
//	return d1;
//}
//
//void Test()
//{
//	Date d1(2018, 9, 26);
//	Date d2(2018, 9, 27);
//	d1 = d1 = d2;
//	d1.Print();
//	cout << (d1 == d2) << endl;
//	cout << d1.operator==(d2) << endl;
//}
//
//int main() {
//	Test();
//	return 0;
//}

//前置++和后置++
//class Date
//{
//public:
//	Date(int year = 1900, int month = 1, int day = 1)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	bool operator==(const Date& d2)
//	{
//		return _year == d2._year
//			&& _month == d2._month
//			&& _day == d2._day;
//	}
//	Date& operator++() {//前置++
//		_day++;
//		return *this;
//	}
//	Date& operator++(int) {//后置++
//		Date temp(*this);
//		_day++;
//		return temp;
//	}
//	//显示日期
//	void Dispaly()const {
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//	int _year;
//	int _month;
//	int _day;
//};
//int main() {
//	Date d1;
//	d1--;
//	--d1;
//
//	return 0;
//}
//const
class Date
{
public:
	Date(int year, int month, int day)
	{
		_year = year;
		_month = month;
		_day = day;
	}
	void Print()
	{
		fun();
		cout << "Print()" << endl;
		cout << "year:" << _year << endl;
		cout << "month:" << _month << endl;
		cout << "day:" << _day << endl << endl;
	}
	void Print() const
	{
		
		cout << "Print()const" << endl;
		cout << "year:" << _year << endl;
		cout << "month:" << _month << endl;
		cout << "day:" << _day << endl << endl;
	}
	void fun()const;
private:
	int _year; // 年
	int _month; // 月
	int _day; // 日
};
void Test()
{
	Date d1(2022, 1, 13);
	d1.Print();//非const对象调用const函数
	const Date d2(2022, 1, 13);
	d2.Print();
}
int main() {
	Test();
}