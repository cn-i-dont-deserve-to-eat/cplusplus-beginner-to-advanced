#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<iostream>
#include<assert.h>

namespace yjl
{
ostream& operator<<(ostream& out, const string& _str);
	class string
	{
		
	private:
		char* _str;
		size_t _size;
		size_t _capacity;


	public:
		/*string(const char* str=" ")
			:_size(strlen(str))
			,_str(new char[strlen(str)+1])
			,_capacity(strlen(str))  // 在初始化列表里面调用太多的strlen！！！
		{

		}*/



		typedef char* iterator;

		// 1.构造函数
		//string(const char* str = nullptr) //这里的str不能给nullptr，因为strlen（nullptr）会出错！
		//string(const char* str = "\0")// 不能是'\0'因为'\0'是char类型，不能赋值给char* 类型的！而"\0"就是空串！ 可以
		string(const char* str = "")// 官方的写法！空串里面就默认有一个'\0'
			:_size(strlen(str)) 

		{
			//cout << _size << endl;
			_str = new char[_size + 1];
			_capacity = _size;
			char* t = _str;
			strcpy(t, str);
			//delete[] _str;
		}

		// 2.析构函数
		~string()
		{
			delete[] _str;
			_str = nullptr;
			_size = _capacity = 0;
			//cout << 111 << endl;
		}


		// 3.下标+[]：遍历
		// (1)capacity模拟
		size_t capacity()
		{
			return _capacity;
		}
		//(2) size()函数的实现！(求string对象的大小)
		size_t size()const // 最好写成const，不仅普通对象可以调用，const对象也可以调用！
		{
			return _size;
		}


		// (3)[]的模拟：返回的是字符(有const，只读！！！)
		const char& operator[](size_t pos)const
		{
			assert(pos < _size);
			return _str[pos];
		}
		// (4)可读可写
		char& operator[](size_t pos)
		{
			assert(pos < _size);
			return _str[pos];
		}
		// 4.模拟迭代器：
		iterator begin()
		{
			return _str;
		}
		iterator end()
		{
			return _str + _size;
		}

		// 5.const 迭代器的模拟
		iterator begin()const
		{
			return _str;
		}
		iterator end()const
		{
			return _str + _size;
		}

		// 6.c_str(将 字符串 转化  字符)
		const char* c_str()
		{
			return _str;
		}

		// 7.reserve 扩容（1）new新空间（2）拷贝字符串（3）释放旧空间（4）指向新空间（5）更新_capacity
		void reserve(size_t n)
		{
			if (n > _capacity)
			{
				// 1.new新空间
				char* tmp = new char[n + 1];
				// 2.拷贝字符串
				strncpy(tmp, _str, _size + 1);
				// 3.释放旧空间
				delete[] _str;
				// 4.指向新空间
				_str = tmp;
				// 5.更新_capacity
				_capacity = n;
			}
		}


		// 8.push_back的模拟！(追加的是一个 字符！)
		void push_back(const char ch)
		{
			// 扩容2倍
			if (_size == _capacity)
			{
				reserve(_capacity == 0 ? 4 : _capacity * 2);
			}
			_str[_size] = ch;
			_size++;
			_str[_size] = '\0';

		}

		// 9.append的模拟！(追加的是一个 字符串！)
		void append(const char* str)
		{
			// 弹性扩容，直接计算str大小
			int n = strlen(str);
			if ((n + _size) > _capacity)
			{
				reserve(n + _size);// 要多少就扩多少！
			}
			strcpy(_str + _size, str);
			_size += n;
		}
		// 10.
		//(1)operator+=重载:字符
		string& operator+=(char ch)
		{
			push_back(ch);
			return *this;
		}
		//(2)operator+=重载:字符串
		string& operator+=(const char* str)
		{
			append(str);
			return *this;
		}
		// 11.insert的模拟：
		// (1)字符
		void insert(size_t pos, char ch)
		{
			assert(pos <= _size);
			// 扩容
			if (_size == _capacity)
			{
				reserve(_capacity == 0 ? 4 : _capacity * 2);
			}

			// 法1：
			//// 下面这个while循环是挪pos后面位置的字符
			//int end = _size;// 你只要记住是 有符号(int) 自动转为 无符号(size_t) 就行了
			//while (end >= (int)pos) // 这里的无符号和有符号进行比较或者运算的时候，
			//					// 直接把这个end的int直接转换成无符号(size_t)，无符号的时候如果pos等于0就会陷入无限循环!
			//{
			//	_str[end + 1] = _str[end];
			//	end--;
			//}
			/*_str[pos] = ch;
			_size++;*/

			// 法2：
			int end = _size + 1;
			while (end > pos)
			{
				_str[end] = _str[end - 1];
				--end;
			}
			_str[pos] = ch;
			_size++;
		}

		// (2)字符串：
		void insert(size_t pos, const char* str)
		{
			assert(pos <= _size);
			int len = strlen(str);
			if (len + _size > _capacity)
			{
				reserve(len + _size);
			}
			int end = len + _size;
			// 这个只是挪动pos后面数据的过程！
			while (end > pos)
			{
				_str[end] = _str[end - len];
				--end;
			}
			// 插入
			strncpy(_str + pos, str, len);
			_size = len + _size;
		}

		// 12.erase的模拟：
		void erase(size_t pos, size_t len = -1)
		{
			assert(pos < _size);
			if (len == -1 || len >= _size - pos)
			{
				_str[pos] = '\0';
				_size = pos;
			}
			else
			{
				strcpy(_str + pos, _str + pos + len);
				_size -= len;
			}

		}

		// 13.swap的模拟：
		void swap(string& s)
		{
			std::swap(_str, s._str);
			std::swap(_size, s._size);
			std::swap(_capacity, s._capacity);
		}

		// 14.find的模拟
		// (1)查找字符！
		size_t find(char ch, size_t pos)const// pos的意义就是：从哪个位置开始查找！
		{
			assert(pos <= _size);
			for (size_t i = pos; i < _size; i++)
			{
				if (_str[i] == ch)
				{
					return i;
				}
				else
				{
					return -1;
				}
			}
		}
		// (2)查找字符串！
		size_t find(char* str, size_t pos)const// pos的意义就是：从哪个位置开始查找！
		{
			assert(pos < _size);
			const char* p = strstr(_str + pos, str);
			if (p)
			{
				return p - _str;
			}
			else
			{
				return -1;
			}
		}

		// 15.substr的模拟：(从字符串中截取一段长度len的子字符串！)
		string substr(size_t pos, size_t len = -1)
		{
			string sub;
			// (1)截取的长度len远超于字符串的总长度_size
			if (len >= _size - pos)
			{
				for (size_t i = pos; i < _size; i++)
				{
					sub += _str[i];
				}
			}
			else
			{
				//for (size_t i = pos; i < _size - pos; i++)
				for (size_t i = pos; i < pos + len; i++)//应该打印的是len个长度的！
				{
					sub += _str[i];
				}
			}

			return sub;
		}

		// 16.清理
		void clear()
		{
			_size = 0;
			_str[_size] = '\0';
		}

		// 17.getline函数的模拟：
		istream& getline(istream& in, string& s)
		{
			//1.先清理
			s.clear();
			char ch;
			// in>>ch
			ch = in.get();

			char buff[128];

			size_t i = 0;
			while (ch != '\n')
			{
				buff[i++] = ch;
				// buff[0,126]

				if (i == 127)
				{
					buff[127] = '\0';
					s += buff;

					i = 0;//???????????????????????????????
				}

				ch = in.get();// ??????????????????????????
			}

			// 走到此处：遇到了'\n' && 没有进去if(i==127)没有把数组填充满！
			if (i > 0)// 但是为啥判断条件是这个？？？i>0？？？？？
			{
				buff[i] = '\0';
				s += buff;
			}

			return in;
		}

		// 18.拷贝构造函数
		// (1) 传统写法：(必须是深拷贝)
		// s1(s2)
		string(const string& s)
		{
			
			// 因为是深拷贝，所以要用new新开辟一块空间
		
				_str = new char[s._capacity + 1];
				strcpy(_str, s._str);
				_size = s._size;
				_capacity = s._capacity;
			//strcpy(_str, s._str);
		}
		//// (2)现代写法：
		//// s1(s2)
		//string(const string& s)
		//{
		//	string tmp = s._str;

		//	//string tmp (s._str);

		//	swap(tmp);
		//}



	};

	// 重载 <<
	

	void test_string1()
	{
		string s1 = "hello world";
		string s2;
		cout << s1 << endl;

		for (size_t i = 0; i < s1.size(); i++)
		{
			cout << s1[i]++ << " ";
		}
		cout << endl;

		for (size_t i = 0; i < s1.size(); i++)
		{
			cout << s1[i] << " ";
		}
		cout << endl;
	}

	void test_string2()
	{
		string s1 = "hello world";
		string::iterator it1 = s1.begin();
		while (it1 != s1.end())
		{
			cout << *it1 << " ";
			it1++;
		}
		cout << endl;
		// 范围for
		for (auto e : s1)
		{
			cout << e << " ";
		}
		cout << endl;
	}

	void test_string3()
	{
		string s1("hello world");
		s1.push_back('1');
		s1.push_back('2');
		s1.push_back('3');
		s1.push_back('4');
		cout << s1 << endl;

		//string s2("xxx");
		s1.append("xxx");

		cout << s1 << endl;
		s1 += "10,9,8,7";
		cout << s1 << endl;
		s1 += '6';
		cout << s1 << endl;

	}

	void test_string4()
	{
		string s1 = "hello world";
		s1.insert(0, 'x');

		cout << s1 << endl;

		s1.insert(4, "ccc");
		cout << s1 << endl;
	}

	void test_string5()
	{
		string s1 = "hello world";
		s1.erase(0, 5);
		cout << s1 << endl;

	}

	void test_string6()
	{
		string s1 = "hello world";
		string s2 = "hello bite";
		cout << s1 << endl;
		cout << s2 << endl;

		swap(s1, s2);
		cout << s1 << endl;
		cout << s2 << endl;
	}

	void test_string7()
	{
		string s1 = "hello world";
		size_t ret1 = s1.find('o', 0);
		cout << ret1 << endl;

		//size_t ret2=s1.find("ld", 0);??????????????
	}

	void test_string8()
	{
		//// 下面演示一下由于浅拷贝(值拷贝)出现的错误问题
		//string s;
		//string s1 = "hello world";
		//s = s1.substr(2, 5); // 这一步 = (赋值)是浅拷贝
		//// s1调用substr函数后的返回值(包括地址)直接浅拷贝给了s
		//// 所以局部变量s的_str和substr的_str的地址是一样的，指向同一空间，在局部变量s析构的时候会与substr的_str冲突
		//
		//cout << s << endl;



		//string s;
		string s1 = "hello world";
		string s = s1.substr(2, 5);

		cout << s << endl;
	}

}