#define _CRT_SECURE_NO_WARNINGS 1
#include<assert.h>
#include<string>
#include<algorithm>
#include<iostream>
namespace bit
{

    template<class T>
    class vector
    {
    public:
        // Vector的迭代器是一个原生指针
        typedef T* iterator;

        typedef const T* const_iterator;
        template<class T> friend void Swap(vector<T>& A, vector<T>& B);

        iterator begin() {
            return _start;
        }

        iterator end() {
            return _finish;
        }
     

        const_iterator begin() const{
            return _start;
        }

        const_iterator end() const {
            return _finish;
        }



            // construct and destroy

        vector() {
            _start = _finish = _endOfStorage = nullptr;
        }

        vector(int n, const T& value = T()) {
            reserve(n);
            for (size_t i = 0; i < n; i++)push_back(value);
        }

            template<class inputiterator>
            vector(inputiterator first, inputiterator last) {
                size_t n = last - first;
                reserve(n);
                memcpy(_start, first, n * sizeof(T));
                _finish = _start + n;
                _endOfStorage = _start + n;

            }

            vector(const vector<T>& v) {
                reserve(v.capacity());
                for (auto& it : v) {
                    push_back(it);
                }
               
             }

            vector<T>& operator= (vector<T> v) {
                reserve(v.capacity());
                memcpy(_start, v._start, v.size() * sizeof(T));
                _finish = _start + v.size();
                return *(this);
             }

                ~vector() {
                    delete[] _start;
                    _start = _finish = _endOfStorage = nullptr;
                }

                // capacity

                size_t size() const {
                    return _finish - _start;
                }

                size_t capacity() const {
                    return _endOfStorage - _start;
                }

                void reserve(size_t n) {
                    if (n>capacity()) {
                        T* temp = new T[n];
                        size_t sz = size();
                      //  memcpy(temp, _start, sz * sizeof(T));

                        for (size_t i = 0; i < sz; i++) {
                            temp[i] = _start[i];
                        }
                        delete[] _start;
                        _start = temp;
                        _finish = _start + sz;
                        _endOfStorage = _start + n;
                    }
                }

                void resize(size_t n, const T& value = T()) {
                    if (n > size()) {
                        reserve(n);
                        T* begin = _start + size();
                        while (begin < _start + n) {
                            *begin = value;
                            begin++;
                        }
                    }
                    else {
                        _finish = _start + n;
                    }
                }



                ///////////////access///////////////////////////////

                T& operator[](size_t pos) {
                    assert(pos < size()); 
                    return _start[pos];
                }

                const T& operator[](size_t pos)const {
                    assert(pos < size());
                    return _start[pos];
                }



                ///////////////modify/////////////////////////////

                void push_back(const T& x) {
                    if (size() == capacity()) {
                        reserve(capacity()==0?4:capacity()*2);
                    }
                    *_finish = x;
                    _finish++;
                }


                void pop_back() {
                    assert(size() > 0);
                    _finish--;
                }

                void Swap(vector<T>& v) {
                    std::swap(v._start, _start);
                    std::swap(v._finish, _finish);
                    std::swap(v._endOfStorage, _endOfStorage);
                }
                bool empty() {
                    return size() == 0;
                }
                iterator insert(iterator pos, const T& x) {
                    assert(pos <= _finish&&pos>=_start);
                    size_t len = pos - _start;
                    reserve(size() + 1);
                    pos = _start + len;
                    iterator end = _finish-1;
                    while (end >= pos) {
                        *(end+1) = *end;
                        end--;
                    }
                    *pos = x;
                    _finish++;
                    return _start;

                }

                iterator erase(iterator pos) {
                    assert(pos < _finish && pos >= _start);
                    iterator begin = pos+1;
                    while (begin <_finish) {
                        *(begin - 1) = *(begin);
                        begin++;
                    }
                    _finish--;
                    return _start;
                }

    private:
        iterator _start; // 指向数据块的开始
        iterator _finish; // 指向有效数据的尾
        iterator _endOfStorage; // 指向存储容量的尾

    };

    template<class T>
    void  Swap(vector<T>& A, vector<T>& B) {
        std::swap(A._start, B._start);
        std::swap(A._finish, B._finish);
        std::swap(A._endOfStorage, B._endOfStorage);
    }

}