#define _CRT_SECURE_NO_WARNINGS 1
#include"vector.h"
#include<iostream>
using namespace bit;
using std::cout;
using std::cin;
using std::endl;
void test1() {
	vector<int> A(5,0);
	vector<int>B;
	B.push_back(1);
	B.push_back(2);
	B.push_back(3);
	B.push_back(4);
	for (int i = 0; i < A.size(); i++) {
		cout << A[i] << " ";
	}
	cout << endl;
	for (auto it : B) {
		cout << it << " ";
	}
	cout << endl;

	Swap(A, B);
	for (int i = 0; i < A.size(); i++) {
		cout << A[i] << " ";
	}
	cout << endl;
	for (auto it : B) {
		cout << it << " ";
	}
	cout << endl;
	//std::cout << std::endl;

}
void test2() {
	vector<int> A(5, 0);
	vector<int>B(A);
    //A.resize(1, 1);
	for (int i = 0; i < A.size(); i++) {
		cout << A[i] << " ";
	}
	cout << endl;
	
	for (auto it : B) {
		cout << it << " ";
	}
	cout << endl;
}
void test3() {
	vector<int> A(5, 0);
	vector<int>B(A);
	A.resize(1, 1);
	for (int i = 0; i < A.size(); i++) {
		cout << A[i] << " ";
	}
	cout << endl;
	A.resize(0, 1);
	for (int i = 0; i < A.size(); i++) {
 		cout << A[i] << " ";
	}
	cout << endl;
	for (auto it : B) {
		cout << it << " ";
	}
	cout << endl;
}

void test4() {
	vector<int> A(5, 0);
	A[0] = 1;
	A[1] = 2;
	A[2] = 3;
	A[3] = 4;
	A.pop_back();
//	A.pop_back();
	A.pop_back();
	A.pop_back();
	for (int i = 0; i < A.size(); i++) {
		cout << A[i] << " ";
	}

}
void test5() {
	vector<int> A;
	A.push_back(1);
	A.push_back(2);
	A.push_back(3);
	A.push_back(4);
	A.push_back(5);
	A.insert(A.begin(), 99);
	A.insert(A.begin()+3, 99);
	A.insert(A.begin()+7, 99);
	for (int i = 0; i < A.size(); i++) {
		cout << A[i] << " ";
	}
	cout << endl;
	A.erase(A.begin());
	A.erase(A.begin());
	A.erase(A.begin());
	A.erase(A.begin());
	A.erase(A.begin()+2);
	for (int i = 0; i < A.size(); i++) {
		cout << A[i] << " ";
	}
}

void test6() {
	vector<std::string> v;
	v.push_back("1111");
	v.push_back("2222");
	v.push_back("3333");
	v.push_back("1111");
	v.push_back("2222");
	v.push_back("3333");
	for (auto it : v) {
		cout << it<< endl;
	}

}
int main() {
	//test1();
	//test2();
	//test3();
	//test4();
	//test5();
	test6();
	return 0;
}