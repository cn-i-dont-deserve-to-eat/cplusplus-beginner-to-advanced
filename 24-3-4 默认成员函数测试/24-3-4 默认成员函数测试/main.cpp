#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

//class Date
//{
//public:
//	void Init(int year, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	void Print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//int main()
//{
//	Date d1;
//	d1.Init(2022, 7, 5);
//	d1.Print();
//	Date d2;
//	d2.Init(2022, 7, 6);
//	d2.Print();
//	return 0;
//}

//构造函数的特性
//
//class Date
//{
//public:
//	Date() {//没有参数的构造函数
//		_year = 0;
//		_month = 0;
//		_day = 0;
//	}
//	Date(int y, int m, int d) {//带参数的构造函数
//		_year = y;
//		_month = m;
//		_day = d;
//	}
//
//	void Print() {
//		cout << _year << " " << _month << " " << _day << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//int main() {
//	Date d1;//调用无参构造
//	Date d2(2024, 3, 4);//调用带参构造
//
//	d1.Print();
//	d2.Print();
//	如果通过无参构造创建对象时，对象后面不用加括号，
//	 因为如果加了括号编译器就会认为你只是在声明一个返回值为Date的函数
//
//	return 0;
//}

//下面代码为什么报错？
//class Date
//{
//public:
//	//Date() {//没有参数的构造函数
//	//	_year = 0;
//	//	_month = 0;
//	//	_day = 0;
//	//}
//	Date(int y, int m, int d) {//带参数的构造函数
//		_year = y;
//		_month = m;
//		_day = d;
//	}
//
//	void Print() {
//		cout << _year << " " << _month << " " << _day << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//int main() {
//	Date d1;//调用无参构造
//
//	d1.Print();
//	return 0;
//}

//默认生成的构造函数对内置类型和自定义类型初始化的区别
//class Time
//{
//public:
//	Time()
//	{
//		cout << "Time()" << endl;
//		_hour = 0;
//		_minute = 0;
//		_second = 0;
//	}
//private:
//	int _hour;
//	int _minute;
//	int _second;
//};
//class Date
//{
//private:
//	// 基本类型(内置类型)
//	int _year;
//	int _month;
//	int _day;
//	// 自定义类型
//	Time _t;
//};
//int main()
//{
//	Date d;
//	return 0;
//}
//

//class Date
//{
//public:
//	Date() {
//		_year = 1900;
//		_month = 0;
//		_day = 0;
//	}
//
//	~Date() {
//		
//	}
//	void Print() {
//		cout << _year << " " << _month << " " << _day << endl;
//	}
//	static void mystaticfunc( Date& t) {
//		t.Print();
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//int main() {
//	Date d1;//调用无参构造
//	Date::mystaticfunc(d1);
//	return 0;
//}

//析构函数
#include<iostream>
using namespace std;
struct Time
{
public:
	Time() {
		_arr = new(int[10]);
	}
	~Time()
	{
		delete _arr;
	}
private:
	int _hour;
	int _minute;
	int _second;
	int* _arr;
};


class Date
{

private:
	// 基本类型(内置类型)
	int _year = 1970;
	int _month = 1;
	int _day = 1;
	// 自定义类型
	Time _t1;
};

void funcation() {
	Date d1;
}

int main() {
	funcation();
	
	return 0;
}