#include <iostream>
#include <cstring> // For memcpy
using namespace std;
//class Buffer {
//public:
//    size_t size;
//    int* data;
//
//    // 构造函数
//    Buffer(size_t s=10) : size(s), data(new int[s]) {
//        std::cout << "构造 " << size * sizeof(int) << " bytes" << std::endl;
//    }
//
//    // 复制构造函数
//    Buffer(const Buffer& other) : size(other.size), data(new int[other.size]) {
//        std::memcpy(data, other.data, size * sizeof(int));
//        std::cout << "拷贝构造 " << size * sizeof(int) << " bytes" << std::endl;
//    }
//    // 移动构造函数
//    Buffer(Buffer&& other) : size(other.size), data(other.data) {
//        other.size = 0;
//        other.data = nullptr;
//        std::cout << "移动构造 " << size * sizeof(int) << " bytes" << std::endl;
//    }
//    //移动赋值
//    Buffer& operator=(Buffer&& other) {
//        if (this != &other) {
//            delete data;//释放当前资源
//            size = other.size;
//            data = other.data;
//            other.data = nullptr;
//            other.size = 0;
//            cout << "移动赋值" << endl;
//        }
//        return *this;
//        
//    }
//
//    //传统赋值拷贝
//    Buffer& operator=(const Buffer& other) {
//        if (this != &other) {
//            Buffer temp(other);//构造一个临时对象
//            delete data;//释放自己的资源
//            data = nullptr;
//            size = 0;
//            swap(temp.data, data);
//            swap(size, temp.size);
//
//        }
//        return *this;
//
//    }
//    // 析构函数
//    ~Buffer() {
//        delete[] data;
//        std::cout << "析构 " << size * sizeof(int) << " bytes" << std::endl;
//    }
//};
//
//void processBuffer(Buffer buf) {
//    // 对缓冲区进行处理
//}

//int main() {
//    Buffer buf1(100);
//   // processBuffer(move(buf1)); // 调用复制构造函数
//    Buffer buf2;
//   // buf2 = buf1;//左值赋值拷贝
//    buf2 = move(buf1);//右值移动赋值
//    return 0;
//}

//void f(int& x) {
//	cout << "x是一个左值" << endl;
//}
//
//void f(int&& x) {
//	cout << "x 是一个右值" << endl;
//}
//
//void getf(int&& x) {
//	f(x);
//}
//
//int main() {
//	int num = 10;
//	getf(move(num));
//	return 0;
//}

//void Fun(int& x) { cout << "左值引用" << endl; }
//void Fun(const int& x) { cout << "const 左值引用" << endl; }
//
//void Fun(int&& x) { cout << "右值引用" << endl; }
//void Fun(const int&& x) { cout << "const 右值引用" << endl; }
//
//template<typename T>
//void PerfectForward(T&& t)//T为万能引用
//{
//	Fun(forward<T>(t));
//}
//int main()
//{
//	PerfectForward(10); //右值
//	int a;
//	PerfectForward(a); //左值
//	PerfectForward(std::move(a)); // 右值
//	const int b = 8;
//	PerfectForward(b); //const 左值
//	PerfectForward(std::move(b)); // const 右值
//	return 0;
//}

class Person
{
public:
	Person()
		
	{}
	Person(Person&& p) {

	}
	Person& operator=(Person&& p)
	{
		cout << "移动赋值" << endl;
		return *this;
	}

};


int main() {
	Person p;
	Person p2 = move(p);
	return 0;
}