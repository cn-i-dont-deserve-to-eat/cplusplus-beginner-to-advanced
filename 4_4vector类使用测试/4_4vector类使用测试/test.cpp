#define _CRT_SECURE_NO_WARNINGS 1
// constructing vectors
#include <iostream>
#include <vector>
using namespace std;



int main() {
	vector<int> A = { 1,2,3,4 };
	vector<int>::iterator it = A.end();
	A.erase(A.end());
	cout << *it << endl;
	return 0;
}




//
//int main() {
//	vector<int> A = { 1,2,3,4,5 };
//
//	vector<int>::iterator it = A.emplace(A.begin(), 0);
//	cout << *it << endl;
//	for (auto t : A) {
//		cout << t << " ";
//	}
//
//	/*A.erase(A.begin());
//	for (auto it : A) {
//		cout << it << " ";
//	}
//	cout << endl;
//	cout << A.capacity() << endl;
//	A.clear();
//	cout << A.capacity() << endl;
//	vector<int> B = { 1,2,3,4,5 };
//	B.erase(B.begin(), B.begin() + 2);
//	for (auto it : B) {
//		cout << it << " ";
//	}
//	cout << endl;*/
//	return 0;
//}
//
////
//int main() {
//	vector<int> A = { 1,2,3,4,5 };
//	vector<int>B = { 9,9,9,9 };
//	A.insert(A.begin(),B.begin(),B.end());
//	for (auto it : A) {
//		cout << it << " ";
//	}
//	cout << endl;
//
//	return 0;
//}


//
//int main() {
//	//vector<int> A = { 1,2,3,4,5,6 };
//	vector<int> B ;
//	B.assign(8, 9);
//	for (auto it : B) {
//		cout << it << " ";
//	}
//
//
//
//	return 0;
//}


//int main() {
//	vector<int>A;
//	int t = A.capacity();
//	for (int i = 0; i < 20; i++) {
//		A.push_back(i);
//		if (A.capacity() != t) {//每次容量发生改变就打印并输出
//			cout << A.capacity() << endl;
//			t = A.capacity();
//		}
//	}
//}




//int main() {
//	vector<int>A = { 1,2,3,4,5 };
//	//vector<int>::reverse_iterator it = A.rbegin();
//	/*while (it != A.rend()) {
//		cout << *it << " ";
//		it++;
//	}*/
//	A.resize(3, 100);
//	for (auto it : A) {
//		cout << it << " ";
//	}
//	
//	//cout << A.size() << " " << A.max_size() << " " << A.capacity() << endl;
//	return 0;
//}
//



//int main()
//{
//    // constructors used in the same order as described above:
//    std::vector<int> first;                                // 无参构造
//    std::vector<int> second(4, 100);                       // 构造的同时初始化4个元素都为100
//    std::vector<int> third(second.begin(), second.end());  // 迭代器初始化构造
//    std::vector<int> fourth(third);                       // 拷贝构造
//
//
//    //迭代器初始化构造示例
//    int myints[] = { 16,2,77,29 };
//    std::vector<int> fifth(myints, myints + sizeof(myints) / sizeof(int));
//
//    std::cout << "The contents of fifth are:";
//    for (std::vector<int>::iterator it = fifth.begin(); it != fifth.end(); ++it)
//        std::cout << ' ' << *it;
//    std::cout << '\n';
//
//    return 0;
//}