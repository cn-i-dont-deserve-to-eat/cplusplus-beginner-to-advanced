#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

typedef void (*VFPTR)();


void PrintVTable(VFPTR vTable[]) {
	//取出数组中的元素依次打印

	int i = 0;
	for (; vTable[i]!=nullptr; i++) {
		printf("第%d个虚函数的地址-> %p ", i, vTable[i]);
		VFPTR f = vTable[i];
		f();//调用这个函数
	}
}