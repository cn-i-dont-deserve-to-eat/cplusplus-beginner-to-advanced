﻿#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<queue>
#include"myvfptr.h"
#include<vector>
using namespace std;


//class A
//{
//
//public:
//
//  void f() { cout << "A::f()" << endl; }
//
//  int a;
//private:
//	int aa;
//
//};
//
//
//
//class B : public A
//
//{
//
//public:
//
//	void f(int a) { cout << "B::f()" << endl; }
//	static int kk;
//	int a;
//
//};
//

//
//class Person {
//public:
//	virtual void BuyTicket() {
//		cout << "买票全价" << endl;
//	}
//};
//
//class Student : public Person {
//public:
//	virtual void BuyTicket() {
//		cout << "买票半价" << endl;
//	}
//};
//
//void fun(Person& people) {
//	people.BuyTicket();
//}
//void test1() {
//	Student s;
//	fun(s);
//
//	Person p;
//	fun(p);
//}
//
//int main()
//{
//	test1();
//	//B b;
//	//b.f();
//	return 0;
//
//}
//
//class Person {
//public:
//	//virtual void fun() {}
//	
//	
//};
//class Student : virtual public Person {
//public:
//	virtual void fun() { 
//
//	}
//};
//
//int main()
//{
//	Student s;
//	cout << sizeof s << endl;
//	//Person* p1 = new Person;
////	Person* p2 = new Student;
//////	delete p1;
////	delete p2;
////	return 0;
//}

//class Car
//{
//public:
//	virtual void Drive() = 0;
//};
//class Benz :public Car
//{
//public:
//	virtual void Drive()
//	{
//		cout << "Benz-舒适" << endl;
//	}
//};
//class BMW :public Car
//{
//public:
//	virtual void Drive()
//	{
//		cout << "BMW-操控" << endl;
//	}
//};
//void Test()
//{
//	Car* pBenz = new Benz;
//	pBenz->Drive();
//	Car* pBMW = new BMW;
//	pBMW->Drive();
//}
//
//class Base
//{
//public:
//	virtual void Func1()
//	{
//		cout << "Func1()" << endl;
//	}
//private:
//	int _b = 1;
//};
//
//int main() {
//	Base b;
//	cout << sizeof b << endl;
//	return 0;
//}

// 针对上面的代码我们做出以下改造
// 1.我们增加一个派生类Derive去继承Base
// 2.Derive中重写Func1
// 3.Base再增加一个虚函数Func2和一个普通函数Func3

//int main()
//{
//	Base b;
//	Derive d;
//	return 0;
//}

//class Person {
//public:
//	virtual void BuyTicket() {
//		cout << "买票全价" << endl;
//	}
//};
//
//class Student : public Person {
//public:
//	virtual void BuyTicket() {
//		cout << "买票半价" << endl;
//	}
//};
//
//void fun(Person people) {
//	people.BuyTicket();
//}
//void test1() {
//	Student s;
//	fun(s);
//
//	Person p;
//	fun(p);
//}
//int main() {
//	test1();
//}


//class Base {
//public:
//	virtual void func1() { cout << "Base::func1" << endl; }
//	virtual void func2() { cout << "Base::func2" << endl; }
//private:
//	int a;
//};
//class Derive :public Base {
//public:
//	virtual void func1() { cout << "Derive::func1" << endl; }
//	virtual void func3() { cout << "Derive::func3" << endl; }
//	virtual void func4() { cout << "Derive::func4" << endl; }
//private:
//	int b;
//};
//
//void test1() {
//	Base b;
//	Derive d;
//	VFPTR* vfp_d = (VFPTR*)(*(int*)&d);//将d的首地址取出来后转换成int*，这样在解引用之后拿到的就是四个字节的值。
//	//再将这首四个字节的值转换为VFPTR*类型（函数指针数组指针）。
//	PrintVTable(vfp_d);
//	cout << "-------------------------" << endl;
//	VFPTR* vfp_b = (VFPTR*)(*(int*)&b);
//	PrintVTable(vfp_b);
//}


class Base {
public:
	 virtual  void func0() { cout << "Base::func0" << endl; }
	
public:
	int b0;
};

class Base1:virtual public Base {
public:
	virtual void func1() { cout << "Base1::func1" << endl; }
	virtual void func2() { cout << "Base1::func2" << endl; }
public:
	int b1;
};
class Base2 :virtual public Base{
public:
	virtual void func1() { cout << "Base2::func1" << endl; }
	virtual void func2() { cout << "Base2::func2" << endl; }
private:
	int b2;
};
class Derive : public Base1, public Base2 {
public:
	virtual void func1() { cout << "Derive::func1" << endl; }
	virtual void func3() { cout << "Derive::func3" << endl; }
private:
	int d1;
};

class A
{

public:

  A() :m_ival(0) { test(); }

  virtual void func() { std::cout << m_ival << " "; }

  void test() { func(); }

public:

  int m_ival;

};



class B : public A

{

public:

	B() { test(); }

	virtual void func()

	{

		++m_ival;

		std::cout << m_ival <<" ";

	}

};



int main(int argc, char* argv[])

{

	A* p = new B;

	p->test();

	return 0;

}
//int main()
//{
//
//	Derive b;
//	
//	cout << sizeof b << endl;
//	VFPTR* vTableb1 = (VFPTR*)(*(int*)&b);
//	PrintVTable(vTableb1);
//
//
//	/*Derive d;
//	VFPTR* vTableb1 = (VFPTR*)(*(int*)&d);
//	PrintVTable(vTableb1);
//	cout << "-------------------" << endl;
//     VFPTR* vTableb2 = (VFPTR*)(*(int*)((char*)&d + sizeof(Base1)));
//	PrintVTable(vTableb2);*/
//	return 0;
//}

