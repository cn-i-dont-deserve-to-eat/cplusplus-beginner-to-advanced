#define _CRT_SECURE_NO_WARNINGS 1
//#include<iostream>
//using namespace std;
//typedef char* pstring;
//
//
//void fun(int a, int b) {
//	cout << a+b << endl;
//}
//int main()
//{
//	void(*p1)(int, int) = fun;
//	p1(1, 2);
//
//	auto p2 = fun;
//	p2(1, 2);
//
//	cout << typeid(p1).name() << endl;
//	cout << typeid(p2).name() << endl;
//	return 0;
//}
#include<iostream>
#include <string>
#include <map>

using namespace std;
//int main()
//{
//	std::map<std::string, std::string> m{ { "apple", "苹果" }, { "orange",
//	"橙子" },
//	{"pear","梨"} };
//	//std::map<std::string, std::string>::iterator it = m.begin();
//	auto it = m.begin();
//	while (it != m.end())
//	{
//		//....
//	}
//	return 0;
//}
//
//int TestAuto()
//{
//	return 10;
//}
//int main()
//{
//	int a = 10;
//	auto b = a;
//	auto c = 'a';
//	auto d = TestAuto();
//	cout << typeid(b).name() << endl;
//	cout << typeid(c).name() << endl;
//	cout << typeid(d).name() << endl;
//	//auto e; 无法通过编译，使用auto定义变量时必须对其进行初始化
//	return 0;
//}
//int main()
//{
//	int x = 10;
//	auto a = &x;
//	auto* b = &x;
//	auto& c = x;
//	cout << typeid(a).name() << endl;
//	cout << typeid(b).name() << endl;
//	cout << typeid(c).name() << endl;
//	*a = 20;
//	cout << x << endl;
//	*b = 30;
//	cout << x << endl;
//	c = 40;
//	cout << x << endl;
//
//	return 0;
//}
//
//void TestAuto()
//{
//	auto a = 1, b = 2;
//	auto c = 3, d = 4.0000; // 该行代码会编译失败，因为c和d的初始化表达式类型不同
//}
//
//int main() {
//	TestAuto();
//	return 0;
//}
//
//void TestAuto(auto a)//不能用作参数
//{
//}
//
//int main() {
//	//TestAuto();
//	int arr[] = { 1, 2, 3, 4, 5 };
//	auto myArray = arr;  // 错误：auto 无法捕获数组的类型
//	cout << typeid(arr).name() << endl;
//	cout << typeid(myArray).name() << endl;
//	return 0;
//}
// 
//范围for
void TestFor()
{
	int array[] = { 1, 2, 3, 4, 5 };
	for (int i = 0; i < sizeof(array) / sizeof(array[0]); ++i)
		array[i] *= 2;
	for (int* p = array; p < array + sizeof(array) / sizeof(array[0]); ++p)
		cout << *p << endl;
}
void TestForauto()
{
	int array[] = { 1, 2, 3, 4, 5 };
	//for(auto& e:array)
	for (int& e : array)
		e *= 2;
	//for(int& e:array)
	for (int e : array)
		cout << e << " ";

}
//void TestForError(int array[])
//{
//	for (auto& e : array)
//		cout << e << endl;
//}
//
//template <typename T, typename U>
//auto multiply(T a, U b) {
//	return a+b;
//}
//
//
//int main() {
//	//TestFor();
//	//TestForauto();
//	//int array[] = { 1, 2, 3, 4, 5 };	
//	//TestForError( array);
//   auto a= multiply(1, 2);
//   cout << a << endl;
//	return 0;
//}

// NULL 和 nullptr 的区别
void TestPtr()
{
	int* p1 = NULL;
	int* p2 = 0;
	// ……
}

//int main() {
//	TestPtr();
//	return 0;
//}
void f(int)
{
	cout << "f(int)" << endl;
}
void f(int*)
{
	cout << "f(int*)" << endl;
}
int main()
{
	f(0);
	f(nullptr);
	f((int*)nullptr);
	return 0;
}