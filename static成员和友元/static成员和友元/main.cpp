#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
//class A
//{
//public:
//	A() {
//		++_scount;
//	}
//	A(const A& t) {
//		++_scount;
//	}
//	~A() {
//		--_scount;
//	}
//	static int GetACount() {
//		return _scount;
//	}
//private:
//	static int _scount;
//};
//
//int A::_scount = 0;//��ʼ��
//
//
//void TestA()
//{
//	cout << A::GetACount() << endl;
//	A a1, a2;
//	A a3(a1);
//	cout << A::GetACount() << endl;
//}
class A
{
public:
	A():_num(10){}
	void Print() {
		B b;
		cout << b._num << endl;
	}
	friend class B;
private:
	int _num;
};

class B
{
public:
	void Print() {
		A a;
		cout << a._num << endl;
	}
private:
	int _num;
};

void test() {
	B b;
	b.Print();
}

int main() {
	test();
	return 0;
}
