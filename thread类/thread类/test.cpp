#define _CRT_SECURE_NO_WARNINGS 1
//#include <iostream>
//#include <thread>
//
//using namespace std;
//void ThreadFunc(int a)
//{
//	cout << "Thread1" << a << endl;
//}
//class TF
//{
//public:
//	void operator()()
//	{
//		cout << "Thread3" << endl;
//	}
//};
//int main()
//{
//	// 线程函数为函数指针
//	thread t1(ThreadFunc, 10);
//	
//	// 线程函数为lambda表达式
//	thread t2([] {cout << "Thread2" << endl; });
//
//	// 线程函数为函数对象
//	TF tf;
//	thread t3(tf);
//
//	t1.join();
//	t2.join();
//	t3.join();
//	cout << "Main thread!" << endl;
//	return 0;
//}
//#include<iostream>
//#include <thread>
//using namespace std;
//void ThreadFunc1(int& x)
//{
//	x += 10;
//	return;
//}
//void ThreadFunc2(int* x)
//{
//	*x += 10;
//}
//int main()
//{
//	int a = 10;
//	thread t1(ThreadFunc1, ref(a));
//	t1.join();
//	//ThreadFunc1(a);
//	cout << a << endl;
//	
//	thread t3(ThreadFunc2, &a);
//	t3.join();
//	cout << a << endl;
//
//	return 0;
//}
//
//#include<iostream>
//#include<thread>
//#include<atomic>
//#include<mutex>
//using namespace std;
//int sum=1;
//mutex g_lock;
//void fun1(size_t num)
//{
//	for (size_t i = 0; i < num; i++) {
//		g_lock.lock();
//		sum++;
//		cout << sum << endl;
//		g_lock.unlock();
//	}
//}
//
//void fun2(size_t num) {
//	for (size_t i = 0; i < num; i++) {
//		g_lock.lock();
//		sum--;
//		cout << sum << endl;
//		g_lock.unlock();
//	}
//}
//
//int main() {
//	thread t1(fun1, 100);
//   
//	thread t2(fun2, 100);
//
//	t1.join();
//	t2.join();
//
//	return 0;
//}
//template<class _Mutex>
//class lock_guard
//{
//public:
//	// 在构造lock_gard时，_Mtx还没有被上锁
//	explicit lock_guard(_Mutex& _Mtx)
//		: _MyMutex(_Mtx)
//	{
//		_MyMutex.lock();
//	}
//	// 在构造lock_gard时，_Mtx已经被上锁，此处不需要再上锁
//	lock_guard(_Mutex& _Mtx, adopt_lock_t)
//		: _MyMutex(_Mtx)
//	{}
//	~lock_guard() _NOEXCEPT
//	{
//		_MyMutex.unlock();
//	}
//	lock_guard(const lock_guard&) = delete;
//	lock_guard& operator=(const lock_guard&) = delete;
//private:
//	_Mutex& _MyMutex;
//};

#include<iostream>
#include<thread>
#include<mutex>
#include<condition_variable>

using namespace std;
mutex mtx;
condition_variable c;
int n = 100;
bool flag = true;

void PrintEven()//打印偶数
{
	int i = 0;
	while (i <= n) {
		unique_lock<mutex> lock(mtx);
		c.wait(lock, [&]() ->bool{return flag; });//flag为true的时候继续打印偶数
		
		cout << i << endl;
		i += 2;
		flag = false;
		c.notify_one();//唤醒
	}
}

void PrintOdd()//打印奇数
{
	int i = 1;
	while (i <= n) {
		unique_lock<mutex> lock(mtx);
		c.wait(lock, [&]() ->bool {return !flag; });//flag为false的时候继续打印奇数
		if (flag) {
			continue;
		}
		cout << i << endl;
		i += 2;
		flag = true;
		c.notify_one();
	}
}
int main() {
	thread t1(PrintEven);
	thread t2(PrintOdd);

	t1.join();
	t2.join();

}